/// <summary>
/// PageExtension "TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJFD_CustomerListExtC01""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" (ID 50151) extends Record Customer List.
/// </summary>
pageextension 50151 D_CustomerListExtC01 extends "Customer List"
{
    actions
    {
        addfirst(General)
        {
            action(TCNJFSwitchVariableC01)
            {
                ApplicationArea = all;
                Caption = 'Prueba variables globales a BC';
                trigger OnAction()
                var
                    culD_FuncionesAppC01: Codeunit "TCNJFD_VariablesGlobalesAppC01";
                begin
                    Message('El valos actual es %1. Lo cambiamos a %2', culD_FuncionesAppC01.GetSwitchF(), not culD_FuncionesAppC01.GetSwitchF());
                    culD_FuncionesAppC01.SetSwitchF(not culD_FuncionesAppC01.GetSwitchF());
                end;
            }
            action(TCNJFPruebaCuCapturarErroresC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba Codeunit de captura de errores';
                trigger OnAction()
                var
                    culD_CapturarErroresC01: Codeunit TCNJFD_CapturarErroresC01;
                begin
                    if culD_CapturarErroresC01.Run() then begin
                        Message('El botón dice que la función se ha ejecutado correctamente');
                    end else begin
                        Message('No se ha podido ejecutar la Codeunit. \El motivo es %1', GetLastErrorText());
                    end;

                end;
            }
            action(TCNJFPrueba02CuCapturarErroresC02C01)
            {
                ApplicationArea = All;
                Caption = 'Prueba 2 Codeunit de captura de errores';
                trigger OnAction()
                var
                    culD_CapturarErroresC01: Codeunit TCNJFD_CapturarErroresC01;
                    i: Integer;
                    xlCodProveedor: Code[20];
                begin
                    xlCodProveedor := '10000';
                    for i := 1 to 5 do begin
                        if culD_CapturarErroresC01.FuncionParaCapturarErrorF(xlCodProveedor) then begin
                            Message('Se ha creado el proveedor %1', xlCodProveedor);
                        end else begin
                            Message('No se ha podido ejecutar la Codeunit. \El motivo es %1', GetLastErrorText());
                        end;
                        xlCodProveedor := IncStr(xlCodProveedor);
                    end;
                end;
            }
        }
        addlast(History)
        {
            action(TCNJFFacturasAbonosT01C01)
            {
                ApplicationArea = All;
                trigger OnAction()
                begin
                    FacturasAbonosF();
                end;
            }
            action(TCNJFExportaClientesC01)
            {
                Caption = 'Exportar Clientes';
                ApplicationArea = All;
                trigger OnAction()
                begin
                    ExportarClientesF();
                end;
            }
            action(TCNJFSumatorioClientesC01)
            {
                Caption = 'Sumatorio01';
                ApplicationArea = All;
                trigger OnAction()
                begin
                    SumarClientesF();
                end;
            }
            action(TCNJFSumatorioClientes02C01)
            {
                Caption = 'Sumatorio02';
                ApplicationArea = All;
                trigger OnAction()
                begin
                    SumarClientes02F();
                end;
            }
            action(TCNJFSumatorioClientes03C01)
            {
                Caption = 'Sumatorio03';
                ApplicationArea = All;
                trigger OnAction()
                begin
                    SumarClientes03F();
                end;
            }
            action(TCNJFSumatorioClientes04C01)
            {
                Caption = 'Sumatorio04';
                ApplicationArea = All;
                trigger OnAction()
                begin
                    SumarClientes04F();
                end;
            }
            action(TCNJFExportarExcelC01)
            {
                Caption = 'Exportar Excel';
                ApplicationArea = All;
                trigger OnAction()
                var
                    TempExcelBuffer: Record "Excel Buffer" temporary;
                    rlCustomer: Record Customer;
                    x: Integer;
                begin
                    TempExcelBuffer.CreateNewBook('Clientes');
                    //Rellenar cabecera
                    AsignaCeldaF(1, 1, Rec.FieldCaption("No."), true, true, TempExcelBuffer);
                    AsignaCeldaF(1, 2, Rec.FieldCaption(Name), true, true, TempExcelBuffer);
                    AsignaCeldaF(1, 3, Rec.FieldCaption("Responsibility Center"), true, true, TempExcelBuffer);
                    AsignaCeldaF(1, 4, Rec.FieldCaption("Ship-to Code"), true, true, TempExcelBuffer);
                    AsignaCeldaF(1, 5, Rec.FieldCaption("Phone No."), true, true, TempExcelBuffer);
                    AsignaCeldaF(1, 6, Rec.FieldCaption(Contact), true, true, TempExcelBuffer);
                    AsignaCeldaF(1, 7, Rec.FieldCaption("Balance (LCY)"), true, true, TempExcelBuffer);
                    AsignaCeldaF(1, 8, Rec.FieldCaption("Balance Due (LCY)"), true, true, TempExcelBuffer);
                    AsignaCeldaF(1, 9, Rec.FieldCaption("Sales (LCY)"), true, true, TempExcelBuffer);
                    AsignaCeldaF(1, 10, Rec.FieldCaption(Payments), true, true, TempExcelBuffer);
                    CurrPage.SetSelectionFilter(rlCustomer);
                    //Rellenar las celdas de excel
                    rlCustomer.SetAutoCalcFields("Balance (LCY)", "Sales (LCY)");
                    rlCustomer.SetLoadFields("No.", Name, "Responsibility Center", "Ship-to Code", "Phone No.",
                    Contact, "Balance (LCY)", "Balance Due (LCY)", "Sales (LCY)", Payments);
                    x := 2;
                    if rlCustomer.FindSet(false) then begin
                        repeat
                            AsignaCeldaF(x, 1, rlCustomer."No.", true, true, TempExcelBuffer);
                            AsignaCeldaF(x, 2, rlCustomer.Name, true, true, TempExcelBuffer);
                            AsignaCeldaF(x, 3, rlCustomer."Responsibility Center", true, true, TempExcelBuffer);
                            AsignaCeldaF(x, 4, rlCustomer."Ship-to Code", true, true, TempExcelBuffer);
                            AsignaCeldaF(x, 5, rlCustomer."Phone No.", true, true, TempExcelBuffer);
                            AsignaCeldaF(x, 6, rlCustomer.Contact, true, true, TempExcelBuffer);
                            AsignaCeldaF(x, 7, Format(rlCustomer."Balance (LCY)"), true, true, TempExcelBuffer);
                            AsignaCeldaF(x, 8, Format(rlCustomer."Balance Due (LCY)"), true, true, TempExcelBuffer);
                            AsignaCeldaF(x, 9, Format(rlCustomer."Sales (LCY)"), true, true, TempExcelBuffer);
                            AsignaCeldaF(x, 10, Format(rlCustomer.Payments), true, true, TempExcelBuffer);
                            x += 1;
                        until rlCustomer.Next() = 0;
                    end;
                    TempExcelBuffer.WriteSheet('', '', '');
                    TempExcelBuffer.CloseBook();
                    TempExcelBuffer.OpenExcel();
                end;
            }
            action(TCNJFImportarExcelC01)
            {
                Caption = 'Importar Excel';
                Image = Import;
                ApplicationArea = All;
                trigger OnAction()
                var
                    TempExcelBuffer: Record "Excel Buffer" temporary;
                    rlCustomer: Record Customer;
                    x: Integer;
                    xlFicheroCte: Text;
                    xlInStr: Instream;
                    xlHoja: Text;
                begin
                    if UploadIntoStream('Seleccione el fichero Excel para importar clientes', '',
                    'Ficheros Excel (*.xls)|*.xls;*.xlsx', xlFicheroCte, xlInStr) then begin
                        xlHoja := TempExcelBuffer.SelectSheetsNameStream(xlInStr);
                        if xlHoja = '' then begin
                            Error('Proceso cancelado');
                        end;
                        TempExcelBuffer.OpenBookStream(xlInStr, xlHoja);
                        TempExcelBuffer.ReadSheet();
                        TempExcelBuffer.SetFilter("Row No.", '>%1', 1);
                        if TempExcelBuffer.FindSet(false) then begin
                            repeat
                                rlCustomer.Next();
                                case TempExcelBuffer."Column No." of
                                    1:
                                        begin
                                            rlCustomer.Init();
                                            rlCustomer.Validate("No.", TempExcelBuffer."Cell Value as Text");
                                        end;
                                    2:
                                        rlCustomer.Validate(Name, TempExcelBuffer."Cell Value as Text");
                                    3:
                                        rlCustomer.Validate("Responsibility Center", TempExcelBuffer."Cell Value as Text");
                                    4:
                                        rlCustomer.Validate("Ship-to Code", TempExcelBuffer."Cell Value as Text");
                                    5:
                                        rlCustomer.Validate("Phone No.", TempExcelBuffer."Cell Value as Text");
                                    6:
                                        rlCustomer.Validate(Contact, TempExcelBuffer."Cell Value as Text");
                                    /*
                                    7:
                                        rlCustomer.Validate("Balance (LCY)", TempExcelBuffer."Cell Value as Text");
                                    8:
                                        rlCustomer.Validate("Balance Due (LCY)", TempExcelBuffer."Cell Value as Text");
                                    9:
                                        rlCustomer.validate("Sales (LCY)", TempExcelBuffer."Cell Value as Text");
                                    10:
                                        rlCustomer.validate(Payments, TempExcelBuffer."Cell Value as Text");
                                    */
                                    11:
                                        rlCustomer.Modify(true);
                                end;
                            until TempExcelBuffer.Next() = 0;
                        end;
                    end else begin
                        Error('No se ha podido subir el fichero');
                    end;
                end;
            }
            action(TCNJFPaginaDeFiltrosC01)
            {
                ApplicationArea = All;
                Caption = 'Página de filtros';
                trigger OnAction()
                var
                    xlPaginaFiltros: FilterPageBuilder;
                    rlCustomer: Record Customer;
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    rlSalesPrice: Record "Sales Price";
                    rlVendor: Record Vendor;
                    clClientes: Label 'Clientes';
                    clMovCte: Label 'Movimientos';
                    clPrecios: Label 'Precios';
                begin
                    xlPaginaFiltros.PageCaption('Seleccione los clientes a procesar');
                    xlPaginaFiltros.AddRecord(clClientes, rlCustomer);
                    xlPaginaFiltros.AddField(clClientes, rlCustomer."No.");
                    xlPaginaFiltros.AddField(clClientes, rlCustomer.TCNJFFechaVacunaC01);
                    xlPaginaFiltros.AddRecord(clMovCte, rlCustLedgerEntry);
                    xlPaginaFiltros.AddField(clMovCte, rlCustLedgerEntry."Posting Date");
                    xlPaginaFiltros.AddRecord(clPrecios, rlSalesPrice);
                    xlPaginaFiltros.AddField(clPrecios, rlSalesPrice."Starting Date");
                    if xlPaginaFiltros.RunModal() then begin
                        Message(xlPaginaFiltros.GetView(clClientes, true));
                        Message(xlPaginaFiltros.GetView(clMovCte, true));
                        Message(xlPaginaFiltros.GetView(clPrecios, true));
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }
        }
    }

    local procedure AsignaCeldaF(pFila: Integer; PColumna: Integer; pContenido: Text; pNegrita: Boolean; pItalica: Boolean; var
    TempPExcelBuffer: Record "Excel Buffer" temporary)
    begin
        TempPExcelBuffer.Init();
        TempPExcelBuffer.Validate("Row No.", pFila);
        TempPExcelBuffer.Validate("Column No.", PColumna);
        TempPExcelBuffer.Insert(true);
        TempPExcelBuffer.Validate("Cell Value as Text", pContenido);
        TempPExcelBuffer.Validate(Bold, pNegrita);
        TempPExcelBuffer.Validate(Italic, pItalica);
        TempPExcelBuffer.Modify(true);
    end;

    local procedure ExportarClientesF()
    var
        TempLConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        xlInStr: InStream;
        xlOutStr: OutStream;
        xlLinea: Text;
        xlNombreFichero: Text;
        clCadenaCabecera: Label 'C%1%2%1%3%1%4%1%5', Locked = true;
        clSeparador: Label '·', Locked = true;
        rlCustomer: Record Customer;
    begin
        TempLConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
        //Escribir el registro
        rlCustomer := Rec;
        rlCustomer.Find();
        rlCustomer.CalcFields("Balance (LCY)");
        xlOutStr.WriteText(StrSubstNo(clCadenaCabecera, clSeparador,
            Rec."No.", Rec.Name, Rec."Balance (LCY)"));
        xlOutStr.WriteText();
        TempLConfiguracionC01.MiBlob.CreateInStream(xlInStr);
        xlNombreFichero := 'Clientes.csv';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('No se ha podido descargar el fichero');
        end;
    end;

    local procedure FacturasAbonosF()
    var
        pglPostedSalesInvoices: Page "Posted Sales Invoices";
        TempLSalesInvoiceHeader: Record "Sales Invoice Header" temporary;
        rlSalesInvoiceHeader: Record "Sales Invoice Header";
        rlSalesCrMemoHeader: Record "Sales Cr.Memo Header";
    begin
        //Recorremos las facturas del cliente
        rlSalesInvoiceHeader.SetRange("Bill-to Customer No.", Rec."No.");
        Message(rlSalesInvoiceHeader.GetFilter("Bill-to Customer No."));
        if rlSalesInvoiceHeader.FindSet(false) then begin
            repeat
                TempLSalesInvoiceHeader.Init();
                TempLSalesInvoiceHeader.TransferFields(rlSalesCrMemoHeader);
                TempLSalesInvoiceHeader.Insert(false);
            until rlSalesCrMemoHeader.Next() = 0;
        end;
        //Recorremos los abonos del cliente
        rlSalesCrMemoHeader.SetRange("Bill-to Customer No.", Rec."No.");
        //Message(rlSalesCrMemoHeader.GetFilters);
        if rlSalesInvoiceHeader.FindSet(false) then begin
            repeat
                TempLSalesInvoiceHeader.Init();
                TempLSalesInvoiceHeader.TransferFields(rlSalesCrMemoHeader);
                TempLSalesInvoiceHeader.Insert(false);
            until rlSalesCrMemoHeader.Next() = 0;
        end;
        Page.Run(0, TempLSalesInvoiceHeader);
    end;

    local procedure SumarClientesF()
    var
        rlDetailedCustLegEntry: Record "Detailed Cust. Ledg. Entry";
        xlTotal: Decimal;
        xlInicio: DateTime;
    begin
        xlInicio := CurrentDateTime;
        rlDetailedCustLegEntry.SetRange("Customer No.", Rec."No.");
        if rlDetailedCustLegEntry.FindSet(false) then begin
            repeat
                xlTotal += rlDetailedCustLegEntry.Amount;
            until rlDetailedCustLegEntry.Next() = 0;
        end;
        Message('Total = %1. Ha tardado %2', xlTotal, CurrentDateTime - xlInicio);
    end;

    local procedure SumarClientes02F()
    var
        rlDetailedCustLegEntry: Record "Detailed Cust. Ledg. Entry";
        xlTotal: Decimal;
        xlInicio: DateTime;
    begin
        xlInicio := CurrentDateTime;
        rlDetailedCustLegEntry.SetRange("Customer No.", Rec."No.");
        rlDetailedCustLegEntry.CalcSums(Amount);
        xlTotal := rlDetailedCustLegEntry.Amount;
        Message('Total = %1. Ha tardado %2', xlTotal, CurrentDateTime - xlInicio);
    end;

    local procedure SumarClientes03F()
    var
        rlCustLegEntry: Record "Cust. Ledger Entry";
        xlTotal: Decimal;
        xlInicio: DateTime;
    begin
        xlInicio := CurrentDateTime;
        rlCustLegEntry.SetRange("Customer No.", Rec."No.");
        rlCustLegEntry.SetLoadFields(Amount);
        rlCustLegEntry.SetAutoCalcFields(Amount);
        if rlCustLegEntry.FindSet(false) then begin
            repeat
                xlTotal += rlCustLegEntry.Amount;
            until rlCustLegEntry.Next() = 0;
        end;
        Message('Total = %1. Ha tardado %2', xlTotal, CurrentDateTime - xlInicio);
    end;

    local procedure SumarClientes04F()
    var
        rlCustLegEntry: Record "Cust. Ledger Entry";
        xlTotal: Decimal;
        xlInicio: DateTime;
    begin
        xlInicio := CurrentDateTime;
        rlCustLegEntry.SetRange("Customer No.", Rec."No.");
        rlCustLegEntry.CalcSums(Amount);
        xlTotal := rlCustLegEntry.Amount;
        Message('Total = %1. Ha tardado %2', xlTotal, CurrentDateTime - xlInicio);
    end;
}
