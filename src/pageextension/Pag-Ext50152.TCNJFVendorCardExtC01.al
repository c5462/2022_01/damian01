pageextension 50152 "TCNJFVendorCardExtC01" extends "Vendor Card"
{
    /*layout
    {
        addlast(content)
        {
            group(TCNJFVacunasC01)
            {
                //Caption = 'Vacunas';

                field(TCNJFNoVacunaC01; Rec.TCNJFNoVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Nº de Vacunas field.';
                    ApplicationArea = All;
                }
                field(TCNJFFechaVacunaC01; Rec.TCNJFFechaVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Fecha ultima Vacuna field.';
                    ApplicationArea = All;
                }
                field(TCNJFTipoVacunaC01; Rec.TCNJFTipoVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Tipo de Vacuna field.';
                    ApplicationArea = All;
                }
            }
        }
    }*/
    actions
    {
        addlast(processing)
        {
            action(TCNJFCreaClienteC01)
            {
                ApplicationArea = All;
                Caption = 'Crea cliente';
                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                    xlConfirm: Boolean;
                begin
                    rlCustomer.SetFilter("No.", Rec."No.");
                    if not rlCustomer.FindSet then begin
                        Message('Se va a añadir un Cliente nuevo');
                        rlCustomer.Init();
                        rlCustomer.TransferFields(Rec, true, true);
                        rlCustomer.Insert(true);
                        Message('Éxito al añadir el cliente, que tenga un buen día');
                    end else begin
                        Message('Le informo de que este cliente ya existe');
                        xlConfirm := Confirm('¿Desea sobreescribirlo? Piensalo bien chaval', false);
                        if xlConfirm then begin
                            rlCustomer.Init();
                            rlCustomer.TransferFields(Rec, true, true);
                            rlCustomer.Modify(false);
                            Message('Cliente sobreescrito, que tenga un buen día');
                        end else begin
                            Message('Proceso cancelado por usted');
                        end;
                    end;
                end;
            }
        }
    }
    var
        culD_FuncionesAppC01: Codeunit "TCNJFD_VariablesGlobalesAppC01";

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        exit(Confirm('¿Desea salir de esta página?', false));
    end;
}