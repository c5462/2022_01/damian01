pageextension 50150 "TCNJFCustomerCardExtC01" extends "Customer Card"
{
    layout
    {
        addlast(content)
        {
            group(TCNJFVacunasC01)
            {
                Caption = 'Vacunas';

                field(TCNJFNoVacunaC01; Rec.TCNJFNoVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Nº de Vacunas field.';
                    ApplicationArea = All;
                }
                field(TCNJFFechaVacunaC01; Rec.TCNJFFechaVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Fecha ultima Vacuna field.';
                    ApplicationArea = All;
                }
                field(TCNJFTipoVacunaC01; Rec.TCNJFTipoVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Tipo de Vacuna field.';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            action(TCNJFJFAsignarVacunaBloqueadaC01)
            {
                ApplicationArea = All;
                Caption = '💉 Asigna vacuna bloqueda 💉';
                trigger OnAction()
                begin
                    //Rec.TCNJFTipoVacunaC01 := 'BLOQUEADA';
                    Rec.Validate(TCNJFTipoVacunaC01, 'BLOQUEADA');
                end;
            }
            action(TCNJFSwitchVariableC01)
            {
                ApplicationArea = all;
                Caption = 'Prueba variables globales a BC';
                trigger OnAction()
                var
                    //culD_FuncionesAppC01: Codeunit TCNJFD_FuncionesAppC01;
                    clMensaje: Label 'El valos actual es %1. Lo cambiamos a %2';
                begin
                    //Message('El valor actual es %1. Lo cambiamos a %2', culD_FuncionesAppC01.GetSwitchF(), not culD_FuncionesAppC01.GetSwitchF());
                    Message(clMensaje, culD_FuncionesAppC01.GetSwitchF(), not culD_FuncionesAppC01.GetSwitchF());
                    culD_FuncionesAppC01.SetSwitchF(not culD_FuncionesAppC01.GetSwitchF());
                end;
            }
            action(TCNJFCreaProveedorC01)
            {
                ApplicationArea = All;
                Caption = 'Crea proveedor';
                trigger OnAction()
                var
                    rlVendor: Record Vendor;
                    xlConfirm: Boolean;
                begin
                    rlVendor.SetFilter("No.", Rec."No.");
                    if not rlVendor.FindSet then begin
                        Message('Se va a añadir un Proveedor nuevo');
                        rlVendor.Init();
                        rlVendor.TransferFields(Rec, true, true);
                        rlVendor.Insert(true);
                        Message('Éxito al añadir el proveedor, que tenga un buen día');
                    end else begin
                        Message('Le informo de que este proveedor ya existe');
                        xlConfirm := Confirm('¿Desea sobreescribirlo? Piensalo bien chaval', false);
                        if xlConfirm then begin
                            rlVendor.Init();
                            rlVendor.TransferFields(Rec, true, true);
                            rlVendor.Modify(false);
                            Message('Proveedor sobreescrito, que tenga un buen día');
                        end else begin
                            Message('Proceso cancelado por usted');
                        end;
                    end;
                end;
            }
        }
    }
    var
        culD_FuncionesAppC01: Codeunit "TCNJFD_VariablesGlobalesAppC01";

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        exit(Confirm('¿Desea salir de esta página?', false));
    end;
}
