pageextension 50153 "TCNJFTCNDCompaniesC01" extends Companies
{
    layout
    {
        addlast(Control1)
        {
            field(TCNJFClientesC01; CLientesF(false))
            {
                ApplicationArea = all;
                Caption = 'Nº clientes';
                DrillDown = true;
                trigger OnDrillDown()
                var
                begin
                    CLientesF(true);
                end;
            }
            field(TCNJFproveedoresC01; ProveedoresF(false))
            {
                ApplicationArea = all;
                Caption = 'Nº Proveedores';
                DrillDown = true;
                trigger OnDrillDown()
                var
                begin
                    ProveedoresF(true);
                end;
            }
            field(TCNJFCuentasC01; CuentasF(false))
            {
                ApplicationArea = all;
                Caption = 'Nº Cuentas';
                DrillDown = true;
                trigger OnDrillDown()
                var
                begin
                    CuentasF(true);
                end;
            }
            field(TCNJFProductosC01; ProductosF(false))
            {
                ApplicationArea = all;
                Caption = 'Nº Productos';
                DrillDown = true;
                trigger OnDrillDown()
                var
                begin
                    ProductosF(true);
                end;
            }

        }

    }
    actions
    {
        addlast(processing)
        {
            action(TCNJFCopiaSeguridadC01)
            {
                ApplicationArea = All;
                Caption = 'Copia seguridad';

                trigger OnAction()
                begin
                    CopiaSeguridadF();
                end;
            }
            action(TCNJFRestaurarCopiaSeguridadC01)
            {
                ApplicationArea = All;
                Caption = 'Restaurar Copia seguridad';

                trigger OnAction()
                begin
                    RestaurarCopiaSeguridadF();
                end;
            }
        }
    }
    local procedure CLientesF(pDrillDown: Boolean): Integer
    var
        rlCustomer: Record Customer;
    begin
        exit(DatosTablaF(rlCustomer, pDrillDown));
    end;

    local procedure ProveedoresF(pDrillDown: Boolean): Integer
    var
        rlVendor: Record Vendor;
    begin
        exit(DatosTablaF(rlVendor, pDrillDown));
    end;

    local procedure CuentasF(pDrillDown: Boolean): Integer
    var
        rlGLAccount: Record "G/L Account";
    begin
        exit(DatosTablaF(rlGLAccount, pDrillDown));
    end;

    local procedure ProductosF(pDrillDown: Boolean): Integer
    var
        rlItem: Record Item;
    begin
        exit(DatosTablaF(rlItem, pDrillDown));
    end;

    local procedure EjemploRecordRefsF()
    var
        xlRecRef: RecordRef;
        xlFieldRef: FieldRef;
        i: Integer;
    begin

        xlRecRef.Open(Database::Item);
        if xlRecRef.FindSet(false) then begin
            repeat
                for i := 1 to xlRecRef.FieldCount do begin
                    Message('%1', xlRecRef.FieldIndex(i).Value);
                end;
                xlRecRef.Field(3).Validate(Format(xlRecRef.Field(3).Value).ToUpper());
                xlFieldRef := xlRecRef.Field(3);
                xlFieldRef.Validate(Format(xlFieldRef.Value).ToUpper());
                xlRecRef.Field(4).Validate('');
                xlRecRef.Modify(true);
            until xlRecRef.Next() = 0;
        end;
        //        xlRec.KeyIndex(1).FieldIndex()
    end;

    local procedure DatosTablaF(pRecVarian: variant; var pDrillDown: Boolean): Integer
    var
        xlRecRef: RecordRef;
        rlAllObjWithCaption: Record AllObjWithCaption;
        rlCompany: Record Company;
    begin
        if rlCompany.Get(Rec.Name) then begin
            if pRecVarian.IsRecord then begin
                xlRecRef.GetTable(pRecVarian);
                if Rec.name <> CompanyName then begin
                    xlRecRef.ChangeCompany(Rec.name);
                end;
                if pDrillDown then begin
                    pRecVarian := xlRecRef;
                    Page.Run(0, pRecVarian);
                end else begin
                    exit(xlRecRef.Count);
                end;
            end;
        end;
    end;

    local procedure ValorCampoF(pFieldRef: FieldRef; pValor: Text): Text
    begin
        if pFieldRef.Type = pFieldRef.Type::Option then begin

        end else begin

        end;
    end;

    local procedure CopiaSeguridadF()
    var
        rlCompany: Record Company;
        rlAllObjWithCaption: Record AllObjWithCaption;
        culTypeHelper: Codeunit "Type Helper";
        xlTextBuilder: TextBuilder;
        xlRecRef: RecordRef;
        i: Integer;
        TempConfC01: Record TCNJFConfiguracionC01 temporary;
        xlOutStr: OutStream;
        xlInStr: InStream;
        xlFichero: Text;
    begin
        //1) Bucle de empresas
        CurrPage.SetSelectionFilter(rlCompany);
        if rlCompany.FindSet(false) then begin
            repeat
                //Escribir en el fichero
                //Inicio empresa
                xlTextBuilder.Append('IE<' + rlCompany.Name + '>' + culTypeHelper.CRLFSeparator());
                //2) Bucle de tablas
                rlAllObjWithCaption.SetRange("Object Type", rlAllObjWithCaption."Object Type"::Table);
                rlAllObjWithCaption.SetFilter("Object ID", '15|18|23|27', Database::"G/L Account", Database::Customer,
                Database::Vendor, Database::Item);
                if rlAllObjWithCaption.FindSet(false) then begin
                    repeat
                        //Inicio tabla
                        xlTextBuilder.Append('IT<' + rlAllObjWithCaption."Object Name" + '>' + culTypeHelper.CRLFSeparator());
                        //3) Bucle de registros
                        xlRecRef.Open(rlAllObjWithCaption."Object ID", false, rlCompany.Name);
                        if xlRecRef.FindSet(false) then begin
                            repeat
                                //Inicio registro
                                xlTextBuilder.Append('IR<' + Format(xlRecRef.RecordId) + '>' + culTypeHelper.CRLFSeparator());
                                //4) Bucle campos
                                for i := 1 to xlRecRef.FieldCount do begin
                                    if xlRecRef.FieldIndex(i).Class <> xlRecRef.FieldIndex(i).Class::FlowFilter then begin
                                        if xlRecRef.FieldIndex(i).Class = xlRecRef.FieldIndex(i).Class::FlowField then begin
                                            xlRecRef.FieldIndex(i).CalcField();
                                        end;
                                        if Format(xlRecRef.FieldIndex(i).Value) <> '' then begin
                                            xlTextBuilder.Append(StrSubstNo('<%1>#<%2>', xlRecRef.FieldIndex(i).Name, xlRecRef.FieldIndex(i).Value) + '<' + culTypeHelper.CRLFSeparator());
                                        end;
                                    end;
                                end;
                                xlTextBuilder.Append('FR<' + Format(xlRecRef.RecordId) + '>' + culTypeHelper.CRLFSeparator());
                            until xlRecRef.Next() = 0;
                        end;
                        xlRecRef.Close();
                        //Fin tabla
                        xlTextBuilder.Append('FT<' + rlAllObjWithCaption."Object Name" + '>' + culTypeHelper.CRLFSeparator());
                    until rlAllObjWithCaption.Next() = 0;
                end;
            until rlCompany.Next() = 0;
        end;
        //Escribir en un contenedor
        TempConfC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
        xlOutStr.WriteText(xlTextBuilder.ToText());
        TempConfC01.MiBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
        xlFichero := 'CopiaSeguridad.bck';
        if not DownloadFromStream(xlInStr, '', '', '', xlFichero) then begin
            Error('No se ha podido descargar el fichero');
        end;
    end;

    local procedure RestaurarCopiaSeguridadF()
    var
        xlInStr: Instream;
        xlLinea: Text;
        TempLConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        xlRecRef: RecordRef;
        NumCampo: Integer;

        rlSalesHeader: Record "Sales Header";
        rlSalesLine: Record "Sales Line";
        clSeparador: Label '·', Locked = true;
        xlFecha: Date;
        xlLNum: Integer;
        xlTipo: Enum "Sales Document Type";
        xlAmount: Decimal;
        xltipoL: Enum "Sales Line Type";
        xlStatus: Enum "Sales Document Status";
        xlEnum: Enum "Acc. Schedule Line Show";
        clError01: Text;
        TempDLogC01: Record TCNJFD_LogC01 temporary;
    begin
        //1) Subir fichero de copias de seguridad
        TempLConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
        if UploadIntoStream('', xlInStr) then begin
            while not xlInStr.EOS do begin //EOS END OF STRING. LEE HASTA EL FINAL.
                //2) Recorrer el fichero
                xlInStr.ReadText(xlLinea);
                case true of
                    //3) Si es inicio de tabla. Abrir el registro
                    xlLinea.StartsWith('IT<'):
                        begin
                            xlRecRef.Open(ObtenNumeroDeTabla(CopyStr(xlLinea, 4).TrimEnd('>'))); //Devuelve el nombre quitando el >
                            //ObtenNumeroDeTabla();//Necesitamos un RecordRef
                        end;
                    //4) Si es fin de tabla, cerrar el registro
                    xlLinea.StartsWith('FT<'):
                        begin
                            xlRecRef.Close();
                        end;
                    //5) Si es inicio de registro, inicializamos el registro
                    xlLinea.StartsWith('IR<'):
                        begin
                            xlRecRef.Init();
                        end;
                    //6) Si es fin de registro, insertar registro
                    xlLinea.StartsWith('FR<'):
                        begin
                            xlRecRef.Insert(false);
                        end;
                    //7) Por cada campo, asignar su valor
                    xlLinea.StartsWith('<'):
                        begin
                            if xlRecRef.Field(NumCampoF(xlRecRef.Number, xlLinea.Split('#').Get(1).TrimStart('<').TrimEnd('>'))).Class
                            = FieldClass::Normal then begin
                                xlRecRef.Field(NumCampoF(xlRecRef.Number, xlLinea.Split('#').Get(1).TrimStart('<').TrimEnd('>'))).Value //Obtiene el numero del campo
                                := CampoYValorF(xlRecRef.Field(NumCampoF(xlRecRef.Number, xlLinea.Split('#').Get(1).TrimStart('<').TrimEnd('>'))), xlLinea.Split('#').Get(2).TrimStart('<').TrimEnd('>'));   //Asignamos el valor
                            end;
                        end;
                end;
            end;
        end else begin
            Error('No se ha podido subir el fichero al servidor. Error (%1)');
        end;

    end;

    local procedure ObtenNumeroDeTabla(pNombreTabla: Text): Integer
    var
        rlAllObjWithCaption: Record AllObjWithCaption; //Método 1
                                                       //rlField: Record Field;    //Método 2 
    begin
        //Método 1
        rlAllObjWithCaption.SetRange("Object Name", pNombreTabla);
        rlAllObjWithCaption.SetRange("Object Type", rlAllObjWithCaption."Object Type"::Table);
        rlAllObjWithCaption.SetLoadFields("Object ID");
        if rlAllObjWithCaption.FindFirst() then begin
            exit(rlAllObjWithCaption."Object ID");
        end;
        /*
        //Método 2
        rlField.SetRange(TableName, pNombreTabla);
        rlField.SetLoadFields(TableNo);
        if rlField.FindFirst() then begin
            exit(rlField.TableNo);
        end;
        */
    end;

    local procedure NumCampoF(pNumTabla: Integer; pNombreCampo: Text): Integer
    var
        rlField: Record Field;
    begin
        rlField.SetRange(TableNo, pNumTabla);
        rlField.SetRange(FieldName, pNombreCampo);
        rlField.SetLoadFields("No.");
        if rlField.FindFirst() then begin
            exit(rlField."No.");
        end;
    end;

    local procedure CampoYValorF(pFieldRef: FieldRef; pValor: Text): Variant
    var
        TempLItem: Record Item temporary;
        TempCustomer: Record Customer temporary;
        TempLConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        xlBigInteger: BigInteger;
        rlField: Record Field;
        i: Integer;
        xlBool: Boolean;
        xlDecimal: Decimal;
        xlDateTime: Time;
        xlDate: Date;
        xlTime: Time;
        xlGuid: Guid;
    begin
        case pFieldRef.Type of
            pFieldRef.Type::BigInteger, pFieldRef.Type::Integer:
                if Evaluate(xlBigInteger, pValor) then begin
                    exit(xlBigInteger);
                end;
            pFieldRef.Type::Option:
                begin
                    for i := 1 to pFieldRef.EnumValueCount() do begin
                        if pFieldRef.GetEnumValueCaption(i).ToLower() = pValor.ToLower() then begin
                            exit(i);
                        end;
                    end;
                end;
            pFieldRef.Type::Boolean:
                if Evaluate(xlBool, pValor) then begin
                    exit(xlBool);
                end;
            pFieldRef.Type::Decimal:
                if Evaluate(xlDecimal, pValor) then begin
                    exit(xlDecimal);
                end;
            pFieldRef.Type::DateTime:
                if Evaluate(xlDateTime, pValor) then begin
                    exit(xlDateTime);
                end;
            pFieldRef.Type::Date:
                if Evaluate(xlDate, pValor) then begin
                    exit(xlDate);
                end;
            pFieldRef.Type::Time:
                if Evaluate(xlTime, pValor) then begin
                    exit(xlTime);
                end;
            pFieldRef.Type::Guid:
                if Evaluate(xlGuid, pValor) then begin
                    exit(xlGuid);
                end;
            pFieldRef.Type::Media, pFieldRef.Type::MediaSet:
                exit(TempCustomer.Image);
            pFieldRef.Type::Blob:
                exit(TempLConfiguracionC01.MiBlob);
            else
                exit(pValor);
        end;
    end;
}