report 50153 "TCNJFlistadoProyectosC01"
{
    Caption = 'Listado de proyectos';
    UsageCategory = Administration;
    ApplicationArea = All;
    RDLCLayout = 'listadoProyectos.rdlc';
    DefaultLayout = RDLC;

    dataset
    {
        dataitem(DataItemName; Job)
        {
            RequestFilterFields = "No.", "Creation Date", "Status";

            column(No_DataItemName; "No.")
            {
            }
            column(BilltoName_DataItemName; "Bill-to Name")
            {
            }
            column(BilltoCustomerNo_DataItemName; "Bill-to Customer No.")
            {
            }

            column(Description_DataItemName; Description)
            {
            }
            column(CreationDate_DataItemName; format("Creation Date"))
            {
            }
            column(Status_DataItemName; Status)
            {
            }
            column(xColor; xColor)
            {

            }
            dataitem("Job Task"; "Job Task")
            {
                RequestFilterFields = "Job No.", "Job Task Type";
                DataItemLink = "Job No." = field("No.");

                column(JobTaskNo_JobTask; "Job Task No.")
                {
                }
                column(JobTaskType_JobTask; "Job Task Type")
                {
                }
                column(Description_JobTask; Description)
                {
                }
                column(StartDate_JobTask; format("Start Date"))
                {
                }
                column(EndDate_JobTask; format("End Date"))
                {
                }
                dataitem("Job Planning Line"; "Job Planning Line")
                {
                    RequestFilterFields = "No.", "Planning Date", type;
                    DataItemLink = "Job No." = field("Job No."), "Job Task No." = field("Job Task No.");
                    column(No_JobPlanningLine; "No.")
                    {
                    }
                    column(Description_JobPlanningLine; Description)
                    {
                    }
                    column(Quantity_JobPlanningLine; Quantity)
                    {
                    }
                    column(PlanningDate_JobPlanningLine; format("Planning Date"))
                    {
                    }
                    column(LineAmount_JobPlanningLine; "Line Amount")
                    {
                    }
                    column(Type_JobPlanningLine; "Type")
                    {
                    }
                    column(LineNo_JobPlanningLine; "Line No.")
                    {
                    }
                    dataitem("Job Ledger Entry"; "Job Ledger Entry")
                    {
                        DataItemLink = "Entry No." = field("Job Ledger Entry No.");

                        column(DocumentNo_JobLedgerEntry; "Document No.")
                        {
                        }
                        column(DocumentDate_JobLedgerEntry; "Document Date")
                        {
                        }

                        column(Type_JobLedgerEntry; "Type")
                        {
                        }
                        column(EntryType_JobLedgerEntry; "Entry Type")
                        {
                        }
                        column(LedgerEntryType_JobLedgerEntry; "Ledger Entry Type")
                        {
                        }
                        column(LineType_JobPlanningLine; "Line Type")
                        {
                        }
                    }

                }
            }
            trigger OnAfterGetRecord()
            begin
                case DataItemName.Status of
                    DataItemName.Status::Completed:
                        begin
                            xColor := 'Green'
                        end;
                    DataItemName.Status::Open:
                        begin
                            xColor := 'Blue'
                        end;
                    DataItemName.Status::Planning:
                        begin
                            xColor := 'Yellow'
                        end;
                    DataItemName.Status::Quote:
                        begin
                            xColor := 'Red'
                        end;


                end;
            end;

        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                }
            }
        }
    }
    var
        xColor: Text;
}