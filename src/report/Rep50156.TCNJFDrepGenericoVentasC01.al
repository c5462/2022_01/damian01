report 50156 "TCNJFD_repGenericoVentasC01"
{
    Caption = 'repGenericoVentas';
    UsageCategory = Administration;
    ApplicationArea = All;
    RDLCLayout = 'genericoVentas.rdlc';
    DefaultLayout = RDLC;

    dataset
    {
        dataitem("Sales Invoice Header"; "Sales Invoice Header")
        {
            RequestFilterFields = "No.";
            dataitem("Sales Invoice Line"; "Sales Invoice Line")
            {
                DataItemLink = "No." = field("No.");
                trigger OnAfterGetRecord()
                var
                    myInt: Integer;
                begin
                    TCNJFLineaGenericaVentasC01.Init();
                    TCNJFLineaGenericaVentasC01.TransferFields("Sales Invoice Line");
                    TCNJFLineaGenericaVentasC01.Insert(false);
                end;
            }
            trigger OnPreDataItem()
            var
                myInt: Integer;
            begin
                if "Sales Invoice Header".GetFilters = '' then begin
                    CurrReport.Skip();
                end;
            end;

            trigger OnAfterGetRecord()
            var
                myInt: Integer;
            begin
                TCND_CabeceraGenericaVentasC01.Init();
                TCND_CabeceraGenericaVentasC01.TransferFields("Sales Invoice Header");
                TCND_CabeceraGenericaVentasC01.Insert(false);
            end;
        }
        dataitem("Sales Shipment Header"; "Sales Invoice Header")
        {
            RequestFilterFields = "No.";
            dataitem("Sales Shipment Line"; "Sales Invoice Line")
            {
                DataItemLink = "No." = field("No.");
                trigger OnAfterGetRecord()
                var
                    myInt: Integer;
                begin
                    TCNJFLineaGenericaVentasC01.Init();
                    TCNJFLineaGenericaVentasC01.TransferFields("Sales Shipment Line");
                    TCNJFLineaGenericaVentasC01.Insert(false);

                    xCaptionInforme := 'Factura'
                end;
            }
            trigger OnPreDataItem()
            var
                myInt: Integer;
            begin
                if "Sales Shipment Header".GetFilters = '' then begin
                    CurrReport.Skip();
                end;
            end;

            trigger OnAfterGetRecord()
            var
                myInt: Integer;
            begin
                TCND_CabeceraGenericaVentasC01.Init();
                TCND_CabeceraGenericaVentasC01.TransferFields("Sales Shipment Header");
                TCND_CabeceraGenericaVentasC01.Insert(false);

                xCaptionInforme := 'Albarán'
            end;
        }
        dataitem(TCND_CabeceraGenericaVentasC01; TCND_CabeceraGenericaVentasC01)
        {
            RequestFilterFields = "No.";
            column(No_TCND_CabeceraGenericaVentasC01; "No.")
            {
            }
            column(SelltoCustomerNo_TCND_CabeceraGenericaVentasC01; "Sell-to Customer No.")
            {
            }
            column(PostingDate_TCND_CabeceraGenericaVentasC01; "Posting Date")
            {
            }
            column(xCaptionInforme; xCaptionInforme)
            {
            }
            dataitem(TCNJFLineaGenericaVentasC01; TCNJFLineaGenericaVentasC01)
            {
                DataItemLink = "No." = field("No.");
                column(No_TCNJFLineaGenericaVentasC01; "No.")
                {
                }
                column(Description_TCNJFLineaGenericaVentasC01; Description)
                {
                }
                column(Quantity_TCNJFLineaGenericaVentasC01; Quantity)
                {
                }
                trigger OnAfterGetRecord()
                var
                    myInt: Integer;
                begin

                end;
            }
            trigger OnAfterGetRecord()
            var
                myInt: Integer;
            begin

            end;
        }
    }
    requestpage
    {
        layout
        {
            area(content)
            {
                group(GroupName)
                {
                }
            }
        }
        actions
        {
            area(processing)
            {
            }
        }
    }

    var
        xCaptionInforme: Text;
}
