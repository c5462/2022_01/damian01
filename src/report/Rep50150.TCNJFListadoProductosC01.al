report 50150 "TCNJFListadoProductosC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'listadoProductos.rdlc';
    AdditionalSearchTerms = 'Listado Productos Ventas/Compras';
    Caption = 'Listado Productos Ventas/Compras';
    dataset
    {
        dataitem(Productos; Item)
        {
            RequestFilterFields = "No.", "Item Category Code";

            column(No_Productos; "No.")
            {
            }
            column(Description_Productos; Description)
            {
            }
            column(ItemCategoryCode_Productos; "Item Category Code")
            {
            }
            dataitem(FacturaVenta; "Sales Invoice Line")
            {
                RequestFilterFields = "Posting Date";
                // DataItemLink = "No." = field("No.");

                column(Quantity_FacturaVenta; Quantity)
                {
                }
                column(UnitPrice_FacturaVenta; "Unit Price")
                {
                }
                column(AmountIncludingVAT_FacturaVenta; "Amount Including VAT")
                {
                }
                column(PostingDate_FacturaVenta; "Posting Date")
                {
                }
                column(Document_No_FacturaVentas; "Document No.") { }
                trigger OnPreDataItem()
                begin
                    FacturaVenta.SetRange("No.", Productos."No.");
                end;

                trigger OnAfterGetRecord()

                begin
                    if not rCustomer.Get(FacturaVenta."Sell-to Customer No.") then begin
                        Clear(rCustomer)

                    end;
                    // rCustomer.SetRange("No.", FacturaVenta."Sell-to Customer No.");
                    // rCustomer.SetFilter("No.", FacturaVenta."Sell-to Customer No." + '&' + FacturaVenta."Sell-to Customer No.");
                    // rCustomer.FindFirst();
                end;

            }
            dataitem(FacturasCompra; "Purch. Inv. Line")
            {

                RequestFilterFields = "Posting Date";
                column(Quantity_FacturasCompra; Quantity)
                {
                }
                column(UnitCost_FacturasCompra; "Unit Cost")
                {
                }
                column(AmountIncludingVAT_FacturasCompra; "Amount Including VAT")
                {
                }
                column(Proveedor; rVendor.Name)
                {

                }
                column(PostingDate_FacturasCompra; "Posting Date")
                {
                }
                column(NombreCliente; rCustomer.Name)
                {

                }
                column(NoCliente; rcustomer."No.")
                {

                }

                column(Document_No_FacturasCompra; "Document No.") { }

                trigger OnPreDataItem()
                begin
                    FacturasCompra.SetRange("No.", Productos."No.");
                end;

                trigger OnAfterGetRecord()
                begin
                    if not rVendor.Get(FacturasCompra."Buy-from Vendor No.") then begin

                        Clear(rVendor);
                    end;

                end;
            }
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }

        actions
        {
            area(processing)
            {
                // action(ActionName)
                // {
                //     ApplicationArea = All;

                // }
            }
        }
    }

    var
        rCustomer: Record Customer;
        rVendor: Record Vendor;
}