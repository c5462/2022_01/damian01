report 50151 "TCNJFFacturaVentaC01"
{
    Caption = 'FacturaVentaC01';
    ApplicationArea = All;
    UsageCategory = Administration;
    DefaultLayout = RDLC;
    RDLCLayout = 'FacturaVenta1.rdlc';
    dataset
    {
        dataitem("Sales Invoice Header"; "Sales Invoice Header")
        {
            column(No_Sales; "No.")
            {

            }
            column(xCustAddr1; xCustAddr[1])
            {

            }
            column(xCustAddr2; xCustAddr[2])
            {

            }
            column(xShipAddr1; xShipAddr[1])
            {

            }
            column(xShipAddr2; xShipAddr[2])
            {

            }
            dataitem(Integer; Integer)
            {
                column(Number; Number)
                {

                }



                dataitem("Sales Invoice Line"; "Sales Invoice Line")
                {
                    column(No_SalesLine; "Line No.")
                    {

                    }
                    trigger OnPreDataItem()

                    begin
                        "Sales Invoice Line".SetRange("Document No.", "Sales Invoice Header"."No.");
                    end;
                }
                trigger OnPreDataItem()

                begin
                    Integer.SetRange(Number, 1, xCopias);
                end;
            }

            trigger OnAfterGetRecord()
            var
                myInt: Integer;
            begin
                cuFormatAddress.SalesInvSellTo(xCustAddr, "Sales Invoice Header");
                cuFormatAddress.SalesInvShipTo(xShipAddr, xCustAddr, "Sales Invoice Header");
                rCompanyInfo.get;
                cuFormatAddress.Company(xCompanyAddr, rCompanyInfo);
            end;
        }
    }
    requestpage
    {
        layout
        {
            area(content)
            {
                group(GroupName)
                {
                    field(xCopias; xCopias)
                    {
                        ApplicationArea = all;
                    }
                }
            }
        }
        actions
        {
            area(processing)
            {
            }
        }
    }
    var
        xCustAddr: array[8] of Text;
        xShipAddr: array[8] of Text;
        xCompanyAddr: array[8] of Text;
        rCompanyInfo: Record "Company Information";
        cuFormatAddress: Codeunit "Format Address";
        xCopias: Integer;
}
