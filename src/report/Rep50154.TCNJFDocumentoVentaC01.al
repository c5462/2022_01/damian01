/// <summary>
/// Report listadoproyectos (ID 50100).
/// </summary>
report 50154 "TCNJFDocumentoVentaC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    //DefaultLayout = Word;
    RDLCLayout = 'DocumentoVentas.rdlc';
    //WordLayout = 'documentoVenta.docx';

    dataset
    {
        dataitem(SalesHeader; "Sales Header")
        {
            RequestFilterFields = "No.", "Document Type";
            column(No_SalesHeader; "No.")
            {
            }
            column(SelltoCustomerNo_SalesHeader; "Sell-to Customer No.")
            {
            }
            column(PostingDate_SalesHeader; "Posting Date")
            {
            }
            column(xDocumentLabel; xDocumentLabel)
            {
            }
            column(EtiquetaDoc; xDocumentLabel)
            {
            }
            column(xCustAddr1; xCustAddr[1])
            {
            }
            column(xCustAddr2; xCustAddr[2])
            {
            }
            column(xCustAddr3; xCustAddr[3])
            {
            }
            column(xCustAddr4; xCustAddr[4])
            {
            }
            column(xCustAddr5; xCustAddr[5])
            {
            }
            column(xCustAddr6; xCustAddr[6])
            {
            }
            column(xCustAddr7; xCustAddr[7])
            {
            }
            column(xCustAddr8; xCustAddr[8])
            {
            }
            column(xCompanyAddr1; xCompanyAddr[1])
            {
            }
            column(xCompanyAddr2; xCompanyAddr[2])
            {
            }
            column(xCompanyAddr3; xCompanyAddr[3])
            {
            }
            column(xCompanyAddr4; xCompanyAddr[4])
            {
            }
            column(xCompanyAddr5; xCompanyAddr[5])
            {
            }
            column(xCompanyAddr6; xCompanyAddr[6])
            {
            }
            column(xCompanyAddr7; xCompanyAddr[7])
            {
            }
            column(xCompanyAddr8; xCompanyAddr[8])
            {
            }
            column(Logo; rCompanyInfo.Picture)
            {
            }
            column(xTotalIVA11; xTotalIVA[1, 1])
            {
            }
            column(xTotalIVA21; xTotalIVA[2, 1])
            {
            }
            column(xTotalIVA31; xTotalIVA[3, 1])
            {
            }
            column(xTotalIVA41; xTotalIVA[4, 1])
            {
            }
            column(xTotalIVA12; xTotalIVA[1, 2])
            {
            }
            column(xTotalIVA22; xTotalIVA[2, 2])
            {
            }
            column(xTotalIVA32; xTotalIVA[3, 2])
            {
            }
            column(xTotalIVA42; xTotalIVA[4, 2])
            {
            }
            column(xTotalIVA13; xTotalIVA[1, 3])
            {
            }
            column(xTotalIVA23; xTotalIVA[2, 3])
            {
            }
            column(xTotalIVA33; xTotalIVA[3, 3])
            {
            }
            column(xTotalIVA43; xTotalIVA[4, 3])
            {
            }
            column(xTotalIVA14; xTotalIVA[1, 4])
            {
            }
            column(xTotalIVA24; xTotalIVA[2, 4])
            {
            }
            column(xTotalIVA34; xTotalIVA[3, 4])
            {
            }
            column(xTotalIVA44; xTotalIVA[4, 4])
            {
            }
            dataitem(Copias; Integer)
            {
                dataitem("Sales Line"; "Sales Line")
                {
                    //DataItemLink = "Document No." = field("No."), "Document Type" = field("Document Type");
                    column(No_SalesLine; "No.")
                    {
                    }
                    column(LineNo_SalesLine; "Line No.")
                    {
                    }
                    column(Description_SalesLine; Description)
                    {
                    }
                    column(Quantity_SalesLine; Quantity)
                    {
                    }
                    column(UnitPrice_SalesLine; "Unit Price")
                    {
                    }
                    column(LineDiscountAmount_SalesLine; "Line Discount Amount")
                    {
                    }
                    column(LineDiscount_SalesLine; "Line Discount %")
                    {
                    }
                    column(Amount_SalesLine; Amount)
                    {
                    }
                    column(Cross_reference_type; "Cross-Reference Type")
                    {
                    }
                    column(Description; Description)
                    {
                    }

                    column(Cross_Reference_No_; "Cross-Reference No.")
                    {
                    }

                    dataitem("Extended Text Line"; "Extended Text Line")
                    {
                        column(Text_ExtendedTextLine; "Text")
                        {
                        }
                    }
                    dataitem("Item Cross Reference"; "Item Cross Reference")
                    {
                        DataItemLink = "Item No." = field("No.");
                        //DataItemTableView = where("Cross_reference_type" = const(customer));
                    }

                    trigger OnPreDataItem()
                    begin
                        "Sales Line".SetRange("Document No.", SalesHeader."No.");
                        "Sales Line".SetRange("Document Type", SalesHeader."Document Type");
                    end;
                }


                trigger OnPreDataItem()
                var
                    xCopias: Integer;
                begin
                    copias.SetRange(Number, 1, xCopias);
                end;
            }


            trigger OnAfterGetRecord()
            var
                myInt: Integer;
            begin
                case SalesHeader."Document Type" of
                    salesheader."Document Type"::Quote:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'Nº Oferta'
                        end;
                    salesheader."Document Type"::Order:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'Nº Pedido'
                        end;
                    salesheader."Document Type"::Invoice:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'Nº Factura'
                        end;
                end;
                CalculoIVA();
            end;
        }
    }
    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(rSalesHeader; rSalesHeader."Document Type")
                    {
                        Caption = 'Tipo de Documento';
                        ApplicationArea = All;
                    }
                    field(xDocumentNo; xDocumentNo)
                    {
                        Caption = 'Número de Documento';
                        ApplicationArea = All;
                        trigger OnLookup(var Text: Text): Boolean
                        var
                            plSalesHeader: Page "Sales List";
                        begin
                            plSalesHeader.SetTableView(rSalesHeader);
                            plSalesHeader.LookupMode := true;
                            if plSalesHeader.RunModal() = Action::LookupOK then begin
                                //Ejecuto página y control de la acción del usuario
                                plSalesHeader.SetSelectionFilter(rSalesHeader); //Aplico seleccion del usuario de la página a la tabla
                                if rSalesHeader.FindSet() then begin
                                    repeat
                                        xDocumentNo += rSalesHeader."No." + '|'; //Elaboración de filtro
                                    until rSalesHeader.Next() = 0;
                                end;
                                xDocumentNo := DelChr(xDocumentNo, '>', '|') //Limpio la concatenación final
                            end;
                        end;
                    }
                    field(Copias1; xCopias)
                    {
                    }
                }
            }
        }
    }

    trigger OnPreReport()
    begin
        rCompanyInfo.Get;
        rCompanyInfo.CalcFields(Picture);
        cuFormatAddress.Company(xCompanyAddr, rCompanyInfo);
    end;

    procedure CalculoIVA()
    var
        rlSalesLine: Record "Sales Line";
        rlTempVATAmount: Record "VAT Amount Line";
        i: Integer;
    begin
        rlSalesLine.SetRange("Document No.", SalesHeader."No.");
        rlSalesLine.SetRange("Document Type", SalesHeader."Document Type");
        if rlSalesLine.FindSet() then begin
            repeat
                rlTempVATAmount.SetRange("VAT Identifier", rlSalesLine."VAT Identifier");
                if rlTempVATAmount.FindFirst() then begin
                    rlTempVATAmount."VAT Base" += rlSalesLine.Amount;
                    rlTempVATAmount."VAT %" := rlSalesLine."VAT %";
                    rlTempVATAmount."VAT Amount" += rlSalesLine."Amount Including VAT" - rlSalesLine.Amount;
                    rlTempVATAmount."Amount Including VAT" += rlSalesLine."Amount Including VAT";
                    rlTempVATAmount."VAT Identifier" := rlSalesLine."VAT Identifier";
                    rlTempVATAmount.Modify(false);
                end else begin
                    rlTempVATAmount.Init();
                    rlTempVATAmount."VAT Base" := rlSalesLine.Amount;
                    rlTempVATAmount."VAT %" := rlSalesLine."VAT %";
                    rlTempVATAmount."VAT Amount" := rlSalesLine."Amount Including VAT" - rlSalesLine.Amount;
                    rlTempVATAmount."Amount Including VAT" := rlSalesLine."Amount Including VAT";
                    rlTempVATAmount."VAT Identifier" := rlSalesLine."VAT Identifier";
                    rlTempVATAmount.Insert(false);
                end;
            //Insert
            //Modify
            until rlSalesLine.Next() = 0;
        end;
        Clear(xTotalIVA);
        rlTempVATAmount.FindSet();
        for i := 1 to 4 do begin
            xTotalIVA[1, i] := Format(rlTempVATAmount."VAT Base");
            xTotalIVA[2, i] := Format(rlTempVATAmount."VAT %");
            xTotalIVA[3, i] := Format(rlTempVATAmount."VAT Amount");
            xTotalIVA[4, i] := Format(rlTempVATAmount."Amount Including VAT");
            if rlTempVATAmount.Next() = 0 then begin
                i := 1000;
            end;
        end;
    end;

    var
        myInt: Integer;
        rSalesHeader: Record "Sales Header";
        xDocumentNo: Text;
        xCustAddr: array[8] of Text;
        xCompanyAddr: array[8] of Text;
        cuFormatAddress: Codeunit "Format Address";
        rCompanyInfo: Record "Company Information";
        xDocumentLabel: Text;
        xCopias: Integer;
        xTotalIVA: array[4, 4] of Text;
}