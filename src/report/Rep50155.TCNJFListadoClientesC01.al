/// <summary>
/// Report TCNJFListadoClientesC01 (ID 50155).
/// </summary>
report 50155 "TCNJFListadoClientesC01"
{
    Caption = 'ListadoClientesProveedores';
    ApplicationArea = All;
    UsageCategory = Administration;
    DefaultLayout = RDLC;
    RDLCLayout = 'ListadoClientesProveedores.rdlc';
    dataset
    {
        dataitem(Cliente; Customer)
        {
            RequestFilterFields = "No.";
            column(NumCliente; "No.")
            {
            }
            column(NombreCliente; Name)
            {
            }
            column(CiudadCliente; City)
            {
            }
            column(AddressCliente; Address)
            {
            }
            column(BilltoCustomerNoCliente; "Bill-to Customer No.")
            {
            }
            column(AmountCliente; Amount)
            {
            }
            dataitem(FacturaVenta; "Sales Invoice Line")
            {
                RequestFilterFields = "Posting Date";
                DataItemLink = "Sell-to Customer No." = field("No.");
            }
        }
        dataitem(Proveedor; Vendor)
        {
            RequestFilterFields = "No.";
            column(NumProveedor; "No.")
            {
            }
            column(NombreProveedor; Name)
            {
            }
            column(BalanceProveedor; Balance)
            {
            }
            column(CiudadProveedor; City)
            {
            }
            column(AddressProveedor; Address)
            {
            }
            dataitem("Purchase Line"; "Purchase Line")
            {
                RequestFilterFields = "Order Date";
                DataItemLink = "Buy-from Vendor No." = field("No.");
                column(Amount_PurchaseLine; Amount)
                {
                }
                column(LineNo_PurchaseLine; "Line No.")
                {
                }
            }
        }
    }
    requestpage
    {
        layout
        {
            area(content)
            {
                group(GroupName)
                {
                }
            }
        }
        actions
        {
            area(processing)
            {
            }
        }
    }
}
