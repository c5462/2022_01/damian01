enum 50160 "TCNJFTipoTablaC01"
{

    value(0; Cliente)
    {
        Caption = 'Tabla de Cliente';
    }
    value(10; Recurso)
    {
        Caption = 'Recurso';
    }
    value(20; Proveedor)
    {
        Caption = 'Proveedor';
    }
    value(30; Empleado)
    {
        Caption = 'Empleado';
    }
    value(40; Contacto)
    {
        Caption = 'Contacto';
    }
}
