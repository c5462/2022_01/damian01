codeunit 50150 "TCNJFFuncionesC01"
{
    procedure NombreTablaF(pTipoTabla: Enum TCNJFTipoTablaC01; pCodTabla: code[20]): Text
    var
        RLTCNJFConfiguracionC01: Record TCNJFConfiguracionC01;
        rlContact: Record Contact;
        rlEmployee: Record Employee;
        rlVendor: Record Vendor;
        rlResource: Record Resource;

    begin
        case
            pTipoTabla of
            pTipoTabla::Cliente:
                begin
                    exit(RLTCNJFConfiguracionC01.NombreClienteF(pCodTabla));
                end;
            pTipoTabla::Contacto:
                begin
                    if rlContact.get(pCodTabla) then begin
                        exit(rlContact.Name);
                    end;
                end;
            pTipoTabla::Empleado:
                begin
                    if rlEmployee.get(pCodTabla) then begin
                        exit(rlEmployee.FullName());
                    end;
                end;
            pTipoTabla::Proveedor:
                begin
                    if rlVendor.get(pCodTabla) then begin
                        exit(rlVendor.Name);
                    end;
                end;
            pTipoTabla::Recurso:
                begin
                    if rlResource.get(pCodTabla) then begin
                        exit(rlResource.Name);
                    end;
                end;
        end
    end;



}
