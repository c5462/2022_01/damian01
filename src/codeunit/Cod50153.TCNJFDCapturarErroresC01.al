/// <summary>
/// Codeunit TCNJFD_CapturarErroresC01 (ID 50153).
/// </summary>
codeunit 50153 "TCNJFD_CapturarErroresC01"
{
    trigger OnRun()
    var
        rlCustomer: Record Customer;
        clCodCte: Label 'AAA', Locked = true, Comment = 'Hola';
    begin
        if not rlCustomer.get(clCodCte) then begin
            rlCustomer.Init();
            rlCustomer.Validate("No.", 'BBB');
            rlCustomer.Insert(true);
            rlCustomer.Validate("Name", 'Proveedor prueba');
            rlCustomer.Validate("Address", 'Calle TAL');
            rlCustomer."VAT Registration No." := 'xxxxx'; //Para que no verifique si el cif está en otro proveedor
            rlCustomer.Validate("Payment Method Code", 'D');
            rlCustomer.Modify(true);
        end;
        Message('Datos del cliente: %1', rlCustomer);
    end;

    /// <summary>
    /// FuncionParaCapturarErrorF.
    /// </summary>
    procedure FuncionParaCapturarErrorF(pCodProvedor: Code[20]): Boolean
    var
        rlVendor: Record Vendor;
    begin
        if not rlVendor.Get(pCodProvedor) then begin
            rlVendor.Init();
            rlVendor.Validate("No.", pCodProvedor);
            rlVendor.Insert(true);
            if AsignarDatosProveedorF(rlVendor) then begin
                rlVendor.Modify(true);
            end else begin
                Message('Error en asignar datos (%1)', GetLastErrorText());
            end;
            exit(true);
        end;
        Message('Ya existe el proveedor %1', pCodProvedor);
    end;

    [TryFunction]
    local procedure AsignarDatosProveedorF(var rlVendor: Record Vendor)
    begin
        rlVendor.Validate("Address", 'Calle TAL');
        rlVendor."VAT Registration No." := 'xxxxx'; //Para que no verifique si el cif está en otro proveedor
        rlVendor.Validate("Payment Method Code", 'D');
    end;
}