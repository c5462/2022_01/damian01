codeunit 50155 "TCNJFD_FuncionesAppC01"
{
    procedure MensajeFraCompraF(PurchInvHdrNo: Code[20])
    begin
        if PurchInvHdrNo <> '' then begin
            Message('El usuario %1 ha creado la factura de compra %2', UserId, PurchInvHdrNo);
        end;
    end;

    procedure FuncionEjemploF(pCodCliente: Code[20]; pParam2: Integer; var prCustomer: Record Customer) return: Text
    var
        i: Integer;
    begin

    end;

    procedure EjemploVariablesESF(var pTexto: Text)
    begin
        pTexto := 'Tecon Servicios';
    end;

    procedure EjemploVariables02ESF(pTexto: TextBuilder)
    begin
        pTexto.Append('Tecon Servicios');
    end;

    procedure EjemploArrayF()
    var
        mlNumeros: Array[5] of Text;
        i: Integer;
        xlNumElementosArray: Integer;
        xlNumRandomObtenido: Integer;
        xlNumElementosEliminadosArray: Integer;
    begin
        for i := 1 to ArrayLen(mlNumeros) do begin
            //mlNumeros[i] := Format(Random(ArrayLen(mlNumeros)));
            mlNumeros[i] := Format(i);
        end;
        repeat
            repeat
                xlNumRandomObtenido := Random(ArrayLen(mlNumeros) - xlNumElementosArray);
            until mlNumeros[xlNumRandomObtenido] <> '';
            Message('La bola que ha salido es %1', mlNumeros[xlNumRandomObtenido]);
            mlNumeros[xlNumRandomObtenido] := '';
            xlNumElementosArray := CompressArray(mlNumeros);
        //xlNumElementosEliminadosArray := ArrayLen(mlNumeros) - xlNumElementosArray;
        until CompressArray(mlNumeros) <= 0;
    end;

    procedure BingoF()
    var
        mlNumeros: Array[20] of Text;
        i: Integer;
        xlNumRandomObtenido: Integer;
    begin
        for i := 1 to ArrayLen(mlNumeros) do begin
            mlNumeros[i] := Format(i);
        end;
        repeat
            xlNumRandomObtenido := Random(CompressArray(mlNumeros));
            Message('La bola que ha salido es %1', mlNumeros[xlNumRandomObtenido]);
            mlNumeros[xlNumRandomObtenido] := '';
        until CompressArray(mlNumeros) <= 0;
    end;

    procedure UltimoNumLineaF(pRec: Record "Sales Line"): Integer
    var
        rlRec: Record "Sales Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end;
    end;

    procedure UltimoNumLineaF(pRec: Record "Purchase Line"): Integer
    var
        rlRec: Record "Purchase Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end;
    end;

    procedure UltimoNumLineaF(pRec: Record "Gen. Journal Line"): Integer
    var
        rlRec: Record "Gen. Journal Line";
    begin
        rlRec.SetCurrentKey("Journal Template Name", "Journal Batch Name", "Line No.");
        rlRec.SetRange("Journal Template Name", pRec."Journal Template Name");
        rlRec.SetRange("Journal Batch Name", pRec."Journal Batch Name");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end;
    end;

    procedure EjemploSobrecargaF(p1: Integer; p2: Decimal; p3: Text)
    begin
        EjemploSobrecargaF(p1, p2, p3, false);
    end;

    procedure EjemploSobrecargaF(p1: Integer; p2: Decimal; p3: Text; pBooleano: Boolean)
    var
        xlNuevoNumero: Decimal;
    begin
        p1 := 1;
        p2 := 12.45;
        p3 := 'Lo que sea';
        //Nueva funcionalidad
        if pBooleano then begin
            xlNuevoNumero := p1 * p2;
        end;
    end;

    procedure UsoFuncionSobrecargaF(p1: Integer; p2: Decimal; p3: Text)
    begin
        EjemploSobrecargaF(10, 34.56, 'Hola mundo');
    end;

    procedure SintaxisF()
    var
        Ventana: Dialog;
        i: Integer;
        xlTexto: Text;
        x: Decimal;
        z: Integer;
        xBool: Boolean;
        xlLong: Integer;
        xlPos: Integer;
        xlCampos: List of [Text];
        clSeparador: Label '·', Locked = true;
        xlFecha: Date;
        rlCustomer: Record Customer;
    begin
        i := 1;
        i := i + 10;
        i += 10;
        xlTexto := 'Jo';
        xlTexto := xlTexto + 'Jo';
        xlTexto += ' XD';
        xlTexto := StrSubstNo('%1 %2 %3', 'Jo', 'Jo', 'XD');
        //Operadores
        i := Round(x + z, 0.0001);
        i := 9 div 2;
        i := 9 mod 2;
        i := Abs(-9);
        xBool := not true;
        xBool := false and true;
        xBool := true and true;
        xBool := true xor true;
        //Operadores reliacionales
        xBool := 1 in [1, 'a', 'V'];
        xBool := 'a' in ['a' .. 'z'];
        //Funciones muy usadas
        Message('Estamos en el año %1%2%1%1', 2, 0);
        Error('Proceso cancelado por el usuario %1', UserId);
        xBool := Confirm('Confirma que desea registrar la factura %1', false, 'FV2022-00001');
        i := StrMenu('Enviar, Facturar, Enviar y Facturar', 3, '¿Cómo desea registrar?');
        case i of
            0:
                Error('Proceso cancelado por el usuario');
            1:
                ;
            2:
                ;
            3:
                ;
        end;
        if GuiAllowed then begin
            Ventana.Open('Procesando bucle i #1###########\' +
                                 'Procesando bucle x #2###########');
        end;

        for i := 1 to 1000 do begin
            if GuiAllowed then begin
                Ventana.Update(1, i);
            end;
            for i := 1 to 1000 do begin
                if GuiAllowed then begin
                    Ventana.Update(2, x);
                end;
            end;
        end;
        Ventana.Close();

        //Funciones de cadenas de texto
        xlLong := MaxStrLen(xlTexto);
        xlLong := MaxStrLen('ABC');
        xlTexto := CopyStr('ABC', 2, 1);    //B
        xlTexto := CopyStr('ABC', 2);       //BC
        xlTexto := xlTexto.Substring(2);
        xlPos := StrPos('Elefante', 'e');   //3 porque hay 3 E y distingue entre mayúsculas y minúsculas
        xlLong := StrLen('ABCD       ');    //4
        xlTexto := xlTexto.ToUpper();
        xlTexto := xlTexto.ToLower();
        xlTexto := UpperCase(xlTexto);
        xlTexto := LowerCase(xlTexto);
        rlCustomer.Validate("Search Name", rlCustomer.Name.ToUpper());
        xlTexto := xlTexto.TrimStart('\').TrimEnd('\').Trim() + '\';
        xlTexto := '123,456,789';
        Message(SelectStr(2, xlTexto)); //456
        xlTexto := '123·456·Tecon Servicios, s.l.·789';
        Message(xlTexto.Split('·').Get(4)); //789
        xlCampos := xlTexto.Split('·');
        Message(xlCampos.Get(4));           //789
        Message(xlTexto.Split(clSeparador).Get(1)); //123
        Message(xlTexto.Split(clSeparador).Get(2)); //456
        Message(xlTexto.Split(clSeparador).Get(3)); //Tecon Servicios, s.l.
        Message(xlTexto.Split(clSeparador).Get(4)); //789

        xlTexto := '21-05-2022';
        Evaluate(xlFecha, ConvertStr(xlTexto, '-', '\'));
        Evaluate(xlFecha, xlTexto.Replace('-', '\').Replace(',', '.'));

        xlTexto := 'Password';
        Message(DelChr(xlTexto, '<=>', 's'));

        xlPos := Power(3, 2);    //9
        xlTexto := UserId();        //Devuelve el usuario
        xlTexto := CompanyName();   //Devuelve la empresa
        Today;
        WorkDate();
        Time;

    end;

    procedure LeftF(pCadena: Text; pNumCaracteres: Integer): Text
    begin
        if pNumCaracteres > StrLen(pCadena) then begin
            Error('Nº caracteres superior a la longitud de la cadena');
        end;
        exit(CopyStr(pCadena, 1, pNumCaracteres));
    end;

    procedure RightF(pCadena: Text; pNumCaracteres: Integer): Text
    begin
        exit(CopyStr(pCadena, (StrLen(pCadena) - pNumCaracteres) + 1));
    end;

    procedure StreamsF()
    var
        TempLConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        xlInStr: InStream;
        xlOutStr: OutStream;
        xlLinea: Text;
        xlNombreFichero: Text;
    begin
        TempLConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::Windows);
        xlOutStr.WriteText('D');
        TempLConfiguracionC01.MiBlob.CreateInStream(xlInStr);
        xlInStr.ReadText(xlLinea);
        xlNombreFichero := 'Pedido.csv';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('El fichero no e ha podido descargar');
        end;
    end;

    local procedure OtrasFuncionesF()
    var
        rlSalesHeader: Record "Sales Header";
        rlSalesInvoiceHeader: Record "Sales Invoice Header";
    begin
        rlSalesInvoiceHeader.TransferFields(rlSalesHeader);
    end;

    local procedure UsoDeListasF()
    var
        xlLista: List of [Code[20]];
    begin
    end;

    procedure AbrirListaClientesF(pNotificacion: Notification)
    var
        rlCustomer: Record Customer;
        clNombreDato: Label 'CodigoCliente';
    begin
        if pNotificacion.HasData('CodigoCliente') then begin
            rlCustomer.SetRange("No.", pNotificacion.GetData(clNombreDato));
        end;
        Page.Run(page::"Customer List", rlCustomer);
    end;

    procedure CodeUnitSalesPostOnAfterPostSalesDocF(var SalesHeader: Record "Sales Header")
    var
        rlCustomer: Record Customer;
        rlSalesLine: Record "Sales Line";
        xlNotificacion: Notification;
    begin
        // 2) Verificar que el cliente tiene vacuna
        if rlCustomer.get(SalesHeader."Sell-to Customer No.") then begin //Para buscar al cliente
            if rlCustomer.TCNJFTipoVacunaC01 = '' then begin
                xlNotificacion.Id(CreateGuid());
                xlNotificacion.Message(StrSubstNo('El cliente %1 no tiene vacuna', rlCustomer."No."));
                xlNotificacion.AddAction('Abrir ficha cliente', Codeunit::TCNJFD_FuncionesAppC01,
                'AbrirFichaClienteF');
                xlNotificacion.SetData('CodCte', rlCustomer."No.");
                xlNotificacion.Send();
            end;
        end;
        // 3) Verificar si sus líneas superan los 100 euros
        rlSalesLine.SetRange("Document Type", SalesHeader."Document Type");
        rlSalesLine.SetRange("Document No.", SalesHeader."No.");
        rlSalesLine.SetFilter(Amount, '<%1', 100);
        rlSalesLine.SetLoadFields("Line No.", "Document No.", Amount);
        if rlSalesLine.FindSet(false) then begin
            repeat
                Clear(xlNotificacion);
                xlNotificacion.Id := CreateGuid();
                xlNotificacion.Message(StrSubstNo('La línea %1 del documento %2 nº %3, con importe %4 es inferior a 100',
                rlSalesLine."Line No.", SalesHeader."Document Type", rlSalesLine."Line No.", rlSalesLine.Amount));
                xlNotificacion.Send();
            until rlSalesLine.Next() = 0;
        end;
    end;

    procedure AbrirFichaClienteF(pNotificacion: Notification)
    var
        rlCustomer: Record Customer;
        clNombreDato: Label 'CodCte';
    begin
        if pNotificacion.HasData(clNombreDato) then begin           //Si hay datos en el cliente
            if rlCustomer.get(pNotificacion.GetData(clNombreDato)) then begin
                Page.Run(page::"Customer Card", rlCustomer);        //Musestra su ficha
            end;
        end;
    end;

    procedure CumpleFiltroF(pCodigo: Code[20]; pFiltro: Text): Boolean
    var
        TempLCustomer: Record Customer temporary;
    begin
        TempLCustomer.Init();
        TempLCustomer."No." := pCodigo;
        TempLCustomer.Insert(false);
        TempLCustomer.SetFilter("No.", pFiltro);
        exit(not TempLCustomer.IsEmpty);
    end;

    local procedure EjemploRecordRefsF()
    var
        xlRecRef: RecordRef;
        xlFieldRef: FieldRef;
        i: Integer;
    begin
        xlRecRef.Open(Database::Item);
        if xlRecRef.FindSet(false) then begin
            repeat
                for i := 1 to xlRecRef.FieldCount do begin
                    Message('%1', xlRecRef.FieldIndex(i).Value);
                end;
                xlRecRef.Field(3).Validate(Format(xlRecRef.Field(3).Value).ToUpper());

                xlFieldRef := xlRecRef.Field(3);
                xlFieldRef.Validate(Format(xlFieldRef.Value).ToUpper());

                xlRecRef.Field(4).Validate();
                xlRecRef.Modify(true);
            until xlRecRef.Next() = 0;
        end;
    end;
}