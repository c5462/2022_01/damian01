codeunit 50154 "TCNJFD_SuscripcionesC01"
{
    SingleInstance = true;
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforePostSalesDoc', '', false, false)]
    local procedure CodeunitSalesPostOnBeforePostSalesDocF(var SalesHeader: Record "Sales Header")
    begin
        SalesHeader.TestField("External Document No.");
    end;

    [EventSubscriber(ObjectType::Table, Database::"Record Link", 'OnAfterValidateEvent', 'URL1', false, false)]
    local procedure DatabaseRecordLinkOnAfterValidateEventURL1F(CurrFieldNo: Integer; var Rec: Record "Record Link"; var xRec: Record "Record Link")
    begin

    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'onAfterPostPurchaseDoc', '', false, false)]
    local procedure CodeunitPurchPostonAfterPostPurchaseDocF(PurchInvHdrNo: Code[20])
    var
        culD_FuncionesAppC01: Codeunit TCNJFD_FuncionesAppC01;
    begin
        culD_FuncionesAppC01.MensajeFraCompraF(PurchInvHdrNo);
    end;

    [EventSubscriber(ObjectType::Table, Database::D_LinPlanVacunacionC01, 'OnBeforeCalcularFechaProximaVacunaF', '', false, false)]
    local procedure DatabaseD_LinPlanVacunacionC01OnBeforeCalcularFechaProximaVacunaF(Rec: Record D_LinPlanVacunacionC01; var isHandled: Boolean)
    begin
        Sleep(5000);    //Se espera 5 segundos
        isHandled := true;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterPostSalesDoc',
    '', false, false)]
    local procedure CodeUnitSalesPostOnAfterPostSalesDocF(var SalesHeader: Record "Sales Header")
    var
        culFuncionesAppC01: Codeunit TCNJFD_FuncionesAppC01;
    begin
        culFuncionesAppC01.CodeUnitSalesPostOnAfterPostSalesDocF(SalesHeader);
    end;
}