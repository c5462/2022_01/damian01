codeunit 50156 "TCNJFD_CodeUnitLentaC01"
{
    trigger OnRun()
    begin
        EjecutarTareaF();
    end;

    local procedure EjecutarTareaF()
    var
        xlEjecutar: Boolean;
    begin
        xlEjecutar := true;
        if GuiAllowed then begin
            xlEjecutar := Confirm('¿Desea ejecutar esta tarea que tarda mucho?');
        end;
        if xlEjecutar then begin
            InsertarLogF('Tarea iniciada');
            Commit();   //No usar casi nunca
            Message('En proceso');
            Sleep(10000);
            InsertarLogF('Tarea finalizada');
        end;
    end;

    local procedure InsertarLogF(pMensaje: Text)
    var
        rlTCNJFD_LogC01: Record TCNJFD_LogC01;
    begin
        rlTCNJFD_LogC01.Init();
        rlTCNJFD_LogC01.Insert(true);
        rlTCNJFD_LogC01.Validate(Texto, pMensaje);
        rlTCNJFD_LogC01.Modify(true);
    end;
}