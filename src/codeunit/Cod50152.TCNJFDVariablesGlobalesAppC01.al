/// <summary>
/// Codeunit TCNJFD_FuncionesAppC01 (ID 50152).
/// </summary>
codeunit 50152 "TCNJFD_VariablesGlobalesAppC01"
{
    SingleInstance = true;
    procedure SetSwitchF(pValor: Boolean)
    begin
        xSwitch := pValor;
    end;

    procedure GetSwitchF() return: Boolean
    begin
        //exit(xSwitch);
        return := xSwitch;
    end;

    /// <summary>
    /// Guarda el nombre a nivel global a BC
    /// </summary>
    /// <param name="pName">Text[100].</param>
    procedure NombreF(pName: Text[100])
    begin
        xName := pName;
    end;

    /// <summary>
    /// Devuelve el nombre guardado globalmente
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure NombreF(): Text
    begin
        exit(xName);
    end;

    var
        xSwitch: Boolean;
        xName: Text;
}