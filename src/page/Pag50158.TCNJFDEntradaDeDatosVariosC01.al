page 50158 "TCNJFD_EntradaDeDatosVariosC01"
{
    Caption = 'Entrada de datos varios';
    PageType = StandardDialog;

    layout
    {
        area(content)
        {
            field(Texto01; mTextos[1])
            {
                ApplicationArea = All;
                Visible = xTxtVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 1];
            }
            field(Texto02; mTextos[2])
            {
                ApplicationArea = All;
                Visible = xTxtVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 2];
            }
            field(Texto03; mTextos[3])
            {
                ApplicationArea = All;
                Visible = xTxtVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 3];
            }
            field(Texto04; mTextos[4])
            {
                ApplicationArea = All;
                Visible = xTxtVisible04;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 4];
            }
            field(Bool01; mBooleanos[1])
            {
                ApplicationArea = All;
                Visible = xBoolVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 1];
            }
            field(Bool02; mBooleanos[2])
            {
                ApplicationArea = All;
                Visible = xBoolVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 2];
            }
            field(Bool03; mBooleanos[3])
            {
                ApplicationArea = All;
                Visible = xBoolVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 3];
            }
            field(Fecha01; mFechas[1])
            {
                ApplicationArea = All;
                Visible = xFechaVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 1];
            }
            field(Fecha02; mFechas[2])
            {
                ApplicationArea = All;
                Visible = xFechaVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 2];
            }
            field(Fecha03; mFechas[3])
            {
                ApplicationArea = All;
                Visible = xFechaVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 3];
            }
            field(Contraseña01; mPass[1])
            {
                ExtendedDatatype = Masked;
                ApplicationArea = All;
                Visible = xContraseñaVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Password, 1];
            }
            field(Contraseña02; mPass[2])
            {
                ExtendedDatatype = Masked;
                ApplicationArea = All;
                Visible = xContraseñaVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Password, 2];
            }
            field(Contraseña03; mPass[3])
            {
                ExtendedDatatype = Masked;
                ApplicationArea = All;
                Visible = xContraseñaVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Password, 3];
            }
        }
    }
    var
        mCaptionClass: array[5, 10] of Text;
        xTipoDato: Option Nulo,Texto,Booleano,Numerico,Hora,Fecha,Password;
        //punteros
        xTipoPuntero: Option Nulo,Pasado,Leido;
        mPunteros: array[2, 5] of Integer;
        //textos
        mTextos: array[10] of Text;
        xTxtVisible01: Boolean;
        xTxtVisible02: Boolean;
        xTxtVisible03: Boolean;
        xTxtVisible04: Boolean;
        //booleanos
        mBooleanos: array[10] of Boolean;
        xBoolVisible01: Boolean;
        xBoolVisible02: Boolean;
        xBoolVisible03: Boolean;
        //Fechas
        mFechas: array[10] of Date;
        xFechaVisible01: Boolean;
        xFechaVisible02: Boolean;
        xFechaVisible03: Boolean;
        //Contraseñas
        mPass: array[10] of Text;
        xContraseñaVisible01: Boolean;
        xContraseñaVisible02: Boolean;
        xContraseñaVisible03: Boolean;

    /// <summary>
    /// Hace que se muestre un campo tipo texto con el caption y valor pasados
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Text) //Textos
    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] += 1;
        mCaptionClass[xTipoDato::Texto, mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto]] := pCaption;
        mTextos[mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto]] := pValorInicial;
        xTxtVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 1;
        xTxtVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 2;
        xTxtVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 3;
        xTxtVisible04 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 4;
    end;
    /// <summary>
    /// Hace que se muestre un campo tipo booleano con el caption y valor pasados.
    /// </summary>
    /// <param name="pCaption">Boolean.</param>
    /// <param name="pValorInicial">Boolean.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Boolean) //Booleanos
    begin
        //Booleanos
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] += 1;
        mCaptionClass[xTipoDato::Booleano, mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano]] := pCaption;
        mBooleanos[mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano]] := pValorInicial;
        xBoolVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 1;
        xBoolVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 2;
        xBoolVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 3;
    end;
    /// <summary>
    /// Hace que se muestre un campo tipo fecha con el caption y valor pasados.
    /// </summary>
    /// <param name="pCaption">Boolean.</param>
    /// <param name="pValorInicial">Boolean.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Date) //Fechas
    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] += 1;
        mCaptionClass[xTipoDato::Fecha, mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha]] := pCaption;
        mFechas[mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha]] := pValorInicial;
        xFechaVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 1;
        xFechaVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 2;
        xFechaVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 3;
    end;
    /// <summary>
    /// Devuelve la contraseña introducido por el usuario
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(pCaption: Text; pValorInicial: Text; pPassword: Boolean) //Contraseñas
    begin
        if pPassword then begin
            mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] += 1;
            mCaptionClass[xTipoDato::Password, mPunteros[xTipoPuntero::Pasado, xTipoDato::Password]] := pCaption;
            mPass[mPunteros[xTipoPuntero::Pasado, xTipoDato::Password]] := pValorInicial;
            xContraseñaVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] >= 1;
            xContraseñaVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] >= 2;
            xContraseñaVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] >= 3;
        end else begin
            CampoF(pCaption, pValorInicial)
        end;

    end;
    /// <summary>
    /// Devuelve el texto introducido por el usuario
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(): Text
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Texto] += 1;
        exit(mTextos[mPunteros[xTipoPuntero::Leido, xTipoDato::Texto]]);
    end;
    ///<summary>
    /// Devuelve el booleano introducido por el usuario
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Boolean): Boolean
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Booleano] += 1;
        pSalida := mBooleanos[mPunteros[xTipoPuntero::Leido, xTipoDato::Booleano]];
        exit(pSalida);
    end;
    ///<summary>
    /// Devuelve la fecha introducido por el usuario
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Date): Date
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Fecha] += 1;
        pSalida := mFechas[mPunteros[xTipoPuntero::Leido, xTipoDato::Fecha]];
        exit(pSalida);
    end;
    ///<summary>
    /// Devuelve la contraseña introducido por el usuario
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Text): Text
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Password] += 1;
        pSalida := mPass[mPunteros[xTipoPuntero::Leido, xTipoDato::Password]];
        exit(pSalida);
    end;


    //Devolviendo valores concretos, le pasas el puntero y te devuelve un valor:
    procedure CampoF(pPuntero: Integer): Text
    begin
        exit(mTextos[pPuntero]);
    end;
    ///<summary>
    /// Devuelve el booleano introducido por el usuario
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Boolean; pPuntero: Integer): Boolean
    begin
        pSalida := mBooleanos[pPuntero];
        exit(pSalida);
    end;
    ///<summary>
    /// Devuelve la fecha introducido por el usuario
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Date; pPuntero: Integer): Date
    begin
        pSalida := mFechas[pPuntero];
        exit(pSalida);
    end;
    ///<summary>
    /// Devuelve la contraseña introducido por el usuario
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Text; pPuntero: Integer): Text
    begin
        pSalida := mPass[pPuntero];
        exit(pSalida);
    end;
}