/// <summary>
/// Page TCNJFConfiguracionC01 (ID 50100).
/// </summary>
page 50150 "TCNJFConfiguracionC01"
{
    Caption = 'Configuracion de la app 01 del curso';
    PageType = Card;
    SourceTable = TCNJFConfiguracionC01;
    AboutTitle = 'En esta página crearemos la configuracion de la primera app del curso de especialista de business central 2022';
    AboutText = 'Fugiat excepteur sunt et culpa consequat. Veniam velit irure aute do ullamco duis aliqua reprehenderit elit cillum pariatur ex. Nulla amet Lorem proident ullamco.';
    AdditionalSearchTerms = 'App de Curso 01';
    ApplicationArea = all;
    UsageCategory = Administration;
    DeleteAllowed = false;
    InsertAllowed = false;
    ShowFilter = true;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(CodClienteWeb; Rec.CodClienteWeb)
                {
                    ToolTip = 'Specifies the value of the Cod cliente para web field.';
                    ApplicationArea = All;
                    AboutTitle = 'En pagina crearemos la configuracion de la primera app del curs de especialistas de business central 2022';
                    AboutText = 'Velit dolore eu reprehenderit amet proident reprehenderit adipisicing ex aute quis. Voluptate fugiat eu nostrud fugiat magna irure. Aute proident anim laboris ad nisi commodo irure enim ullamco. Nostrud laboris veniam excepteur est deserunt exercitation anim dolore anim irure nisi exercitation sunt. Ea minim voluptate nostrud fugiat reprehenderit culpa sint est voluptate cupidatat ea officia dolor. Exercitation et magna ea consequat ex ad esse ut adipisicing ipsum qui dolore veniam eu. Est qui sunt et nostrud.';
                    Importance = Promoted;

                }

                field(TextoRegistro; Rec.TextoRegistro)
                {
                    ToolTip = 'Specifies the value of the Texto registro field.';
                    ApplicationArea = All;
                }
                field(TipoDoc; Rec.TipoDoc)
                {
                    ToolTip = 'Specifies the value of the Tipo documento field.';
                    ApplicationArea = All;
                }
            }
            group(InfInterna)
            {
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                }
                field(SystemId; Rec.SystemId)
                {
                    ToolTip = 'Specifies the value of the SystemId field.';
                    ApplicationArea = All;
                    Importance = Additional;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                }
            }
            group(CampoCalculado)
            {
                Caption = 'Campos calculados';
                field(NombreCliente; Rec.NombreCliente)
                {
                    ToolTip = 'Muestra nombre del cliente';
                    ApplicationArea = All;
                    Importance = Additional;
                }
                field(NombreClienteFuncion; Rec.NombreClienteF(Rec.CodClienteWeb))
                {
                    Caption = 'Nombre cliente';
                    ApplicationArea = All;
                }
            }
            group(TablaPrueba)
            {
                field(TipoTabla; Rec.TipoTabla)
                {
                    ToolTip = 'Specifies the value of the TipoTabla field.';
                    ApplicationArea = All;
                }
                field(CodTabla; Rec.CodTabla)
                {
                    ToolTip = 'Specifies the value of the CodTabla field.';
                    ApplicationArea = All;
                }
                field(NombreTabla; Rec.NombreTablaF(Rec.TipoTabla, Rec.CodTabla))
                {
                    Caption = 'Nombre de la tabla';
                    ApplicationArea = All;
                    Editable = false;
                }
                field(ColorFondo; Rec.ColorFondo)
                {
                    Caption = 'Color fondo';
                    ToolTip = 'Specifies the value of the ColorFondo field.';
                    ApplicationArea = All;
                }
                field(Password; xPassword)
                {
                    Caption = 'Contraseña';
                    ToolTip = 'Specifies the value of the ColorFondo field.';
                    ApplicationArea = All;
                }
                field(ColorLetra; Rec.ColorLetra)
                {
                    Caption = 'Color letra';
                    ToolTip = 'Specifies the value of the ColorFondo field.';
                    ApplicationArea = All;
                }
                field(NumUnidadesVendidas; Rec.UnidadesDisponibles)
                {
                    Caption = 'Unidades disponiles';
                    ToolTip = 'Specifies the value of the Nº de unidades vendidas field.';
                    ApplicationArea = All;
                }
                field(NumIteraciones; xNumIteraciones)
                {
                    Caption = 'Numero de iteraciones';
                    ToolTip = 'Specifies the value of the Nº de unidades vendidas field.';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Ejemplo01)
            {
                ApplicationArea = all;
                Caption = 'Mensaje de accion';
                Image = AboutNav;
                trigger OnAction()
                begin
                    Message('Accion pulsada');
                end;
            }
            action(PedirDatos)
            {
                ApplicationArea = All;
                Caption = 'Pedir datos';
                trigger OnAction()
                var
                    pglD_EntradaDeDatosVariosC01: Page TCNJFD_EntradaDeDatosVariosC01;
                    xlBool: Boolean;
                    xlDate: Date;
                begin
                    pglD_EntradaDeDatosVariosC01.CampoF('Ciudad de nacimiento', 'Albacete');
                    pglD_EntradaDeDatosVariosC01.CampoF('País de nacimiento', '');
                    pglD_EntradaDeDatosVariosC01.CampoF('Comunidad de nacimiento', 'CLM');
                    //pglD_EntradaDeDatosVariosC01.CampoF('Cuota pagada', false);
                    pglD_EntradaDeDatosVariosC01.CampoF('Cuota 1er trimestre pagada', false);
                    pglD_EntradaDeDatosVariosC01.CampoF('Cuota 2º trimestre pagada', true);
                    pglD_EntradaDeDatosVariosC01.CampoF('Fecha 1: ', Today);
                    pglD_EntradaDeDatosVariosC01.CampoF('Fecha 2: ', Today);
                    if pglD_EntradaDeDatosVariosC01.RunModal() in [Action::OK, Action::Yes, Action::LookupOK] then begin
                        //Message('Acción aceptada');
                        Message(pglD_EntradaDeDatosVariosC01.CampoF());
                        Message(pglD_EntradaDeDatosVariosC01.CampoF());
                        Message(pglD_EntradaDeDatosVariosC01.CampoF());
                        Message('Cuota 1 %1', pglD_EntradaDeDatosVariosC01.CampoF(xlBool));
                        pglD_EntradaDeDatosVariosC01.CampoF(xlBool);
                        Message('Cuota 2 %1', xlBool);
                        Message('Fecha 1 %1', pglD_EntradaDeDatosVariosC01.CampoF(xlDate));
                        pglD_EntradaDeDatosVariosC01.CampoF(xlDate);
                        Message('Fecha 2 %1', xlDate);
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }
            action(CambiarPassword)
            {
                ApplicationArea = All;
                Caption = 'Cambiar Password';
                trigger OnAction()
                begin
                    CambiarPasswordF();
                end;
            }
            action(ExportarVacunas)
            {
                ApplicationArea = All;
                Caption = 'Exportar vacunas';
                trigger OnAction()
                begin
                    ExportarVacunasF();
                end;
            }
            action(ImportarVacunas)
            {
                ApplicationArea = All;
                Caption = 'Importar vacunas';
                trigger OnAction()
                begin
                    ImportarVacunasF();
                end;
            }
            action(FiltraClientes)
            {
                ApplicationArea = All;
                Caption = 'Clientes de almacen gris';
                trigger OnAction()
                begin
                    ClientesGrisF();
                end;
            }
            action(PruebaTxt)
            {
                ApplicationArea = All;
                Caption = 'Pruebas con Text';
                trigger OnAction()
                var
                    xlInicio: DateTime;
                    i: Integer;
                    xlTxt: Text;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to xNumIteraciones do begin
                        xlTxt += '.';
                    end;
                    Message('Tiempo invertido: %1 %2', CurrentDateTime - xlInicio, xlTxt);
                end;
            }
            action(PruebaTextBuilder)
            {
                ApplicationArea = All;
                Caption = 'Pruebas con Text';
                trigger OnAction()
                var
                    xlInicio: DateTime;
                    i: Integer;
                    xlTxt: TextBuilder;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to xNumIteraciones do begin
                        xlTxt.Append('.');
                    end;
                    Message('Tiempo invertido: %1 %2', CurrentDateTime - xlInicio, xlTxt.ToText());
                end;
            }
            action(ContarClientes)
            {
                ApplicationArea = All;
                Caption = 'Contar clientes';
                trigger OnAction()
                var
                    xlPaginaFiltros: FilterPageBuilder;
                    rlCustLedEntry: Record "Cust. Ledger Entry";
                    rlCustomer: Record Customer;
                    xlCLientes: List of [Code[20]];
                    clClientes: Label 'Clientes';
                    clMovClientes: Label 'Movimientos del cliente';
                    xlDic: Dictionary of [Code[20], Decimal];
                begin
                    xlPaginaFiltros.PageCaption('Seleccione usted los registros a contar si es tan amable');
                    xlPaginaFiltros.AddRecord(clMovClientes, rlCustLedEntry);
                    xlPaginaFiltros.AddField(clMovClientes, rlCustLedEntry."Posting Date");
                    if xlPaginaFiltros.RunModal() then begin
                        rlCustLedEntry.SetView(xlPaginaFiltros.GetView(clMovClientes));
                        rlCustLedEntry.SetLoadFields("Customer No.");
                        if rlCustLedEntry.FindSet(false) then begin
                            repeat
                                if xlCLientes.Contains(rlCustLedEntry."Customer No.") then begin
                                    xlClientes.Add(rlCustLedEntry."Customer No.");
                                end;
                            until rlCustLedEntry.Next() = 0;
                        end else begin
                            Message('No existen registros dentro del filtro %1', rlCustLedEntry.GetFilters);
                        end;
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }
            action(ExportarPedidosVentas)
            {
                ApplicationArea = All;
                Caption = 'Exportar pedidos venta';
                trigger OnAction()
                begin
                    ExportarPedidosVentaF();
                end;
            }
            action(TCNJFImportarPedidosVentaC01)
            {
                Caption = 'Importar Pedidos venta';
                Image = Import;
                ApplicationArea = All;
                trigger OnAction()
                begin
                    ImportarPedidosVentaF();
                end;
            }
            action(ExportarPedidosVentasNiko)
            {
                ApplicationArea = All;
                Caption = 'Exportar pedidos venta (Niko version)';
                trigger OnAction()
                begin
                    ExportarPedidosVentaNikoF();
                end;
            }
            action(TCNJFImportarPedidosVentaNikoC01)
            {
                Caption = 'Importar Pedidos venta (Niko version)';
                Image = Import;
                ApplicationArea = All;
                trigger OnAction()
                begin
                    ImportarPedidosVentaNikoF();
                end;
            }
            action(TCNJFListaEmpresasC01)
            {
                Caption = 'Lista empresas';
                Image = Import;
                ApplicationArea = All;
                trigger OnAction()
                begin
                    MuestraEmpresasF();
                end;
            }
        }
    }
    trigger OnOpenPage()
    begin
        Rec.GetF();
    end;

    [NonDebuggable]
    local procedure CambiarPasswordF()
    var
        pgD_EntradaDeDatosVariosC01: Page TCNJFD_EntradaDeDatosVariosC01;
        xlPass: Text;
    begin
        pgD_EntradaDeDatosVariosC01.CampoF('Password actual', '', true);
        pgD_EntradaDeDatosVariosC01.CampoF('Nuevo password', '', true);
        pgD_EntradaDeDatosVariosC01.CampoF('Repita la contraseña', '', true);
        if pgD_EntradaDeDatosVariosC01.RunModal() in [Action::OK, Action::Yes, Action::LookupOK] then begin
            if pgD_EntradaDeDatosVariosC01.CampoF(xlPass) = Rec.PasswordF() then begin
                if pgD_EntradaDeDatosVariosC01.CampoF(xlPass) = pgD_EntradaDeDatosVariosC01.CampoF(xlPass) then begin
                    Rec.PasswordF(xlPass);
                    Message('Password cambiado');
                end else begin
                    Error('Nuevo password no coincide');
                end;
            end else begin
                Error('Nuevo password no coincide');
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
    end;

    local procedure ExportarVacunasF()
    var
        //rlCabPlanVacunacionC01: Record TCNJFD_CabPlanVacunacionC01;
        rlCabPlanVacunacionC01: Record D_CabPlanVacunacionC01;
        pgListaCabPlanVacunacionC01: Page D_ListaCabPlanVacunacionC01;
        rlLinPlanVacunacion: Record D_LinPlanVacunacionC01;
        TempLConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        xlInStr: InStream;
        xlOutStr: OutStream;
        xlLinea: Text;
        xlNombreFichero: Text;
        clCadenaCabecera: Label 'C%1%2%1%3%1%4%1%5', Locked = true;
        clCadenaLinea: Label 'C%1%2%1%3%1%4%1%5%1%6%1%7%1%8', Locked = true;
        clSeparador: Label '·', Locked = true;
    begin
        TempLConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
        ;
        Message('Seleccione vacunas a exportar');
        pgListaCabPlanVacunacionC01.LookupMode(true);
        pgListaCabPlanVacunacionC01.SetRecord(rlCabPlanVacunacionC01);
        if pgListaCabPlanVacunacionC01.RunModal() in [Action::OK, Action::Yes, Action::LookupOK] then begin
            pgListaCabPlanVacunacionC01.GetSelectionFilterF(rlCabPlanVacunacionC01);
            //Obtenemos el dataset
            if rlCabPlanVacunacionC01.FindSet(false) then begin
                repeat
                    xlOutStr.WriteText(StrSubstNo(clCadenaCabecera, clSeparador,
                        rlCabPlanVacunacionC01.CodigoCabecera, clSeparador,
                        rlCabPlanVacunacionC01.Descripcion, clSeparador,
                        rlCabPlanVacunacionC01.EmpresaVacunadora, clSeparador,
                        rlCabPlanVacunacionC01.FechaInicioPlanifcVacunacion));
                    xlOutStr.WriteText();
                    //Recorremos las líneas de vacuna para exportarlas
                    rlLinPlanVacunacion.SetRange(CodigoLineas, rlCabPlanVacunacionC01.CodigoCabecera);
                    if rlLinPlanVacunacion.FindSet(false) then begin
                        repeat
                            xlOutStr.WriteText(StrSubstNo(clCadenaLinea, clSeparador,
                                rlLinPlanVacunacion.CodigoLineas, clSeparador,
                                rlLinPlanVacunacion.CodCteAVacunar, clSeparador,
                                rlLinPlanVacunacion.CodigoOtros, clSeparador,
                                rlLinPlanVacunacion.CodigoVacuna, clSeparador,
                                rlLinPlanVacunacion.FechaVacunacion, clSeparador,
                                rlLinPlanVacunacion.FechaProximaVacunacion, clSeparador,
                                Format(rlLinPlanVacunacion.NumLinea)));
                            xlOutStr.WriteText();
                        until rlLinPlanVacunacion.Next() = 0;
                    end;
                until rlCabPlanVacunacionC01.Next() = 0;
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
        TempLConfiguracionC01.MiBlob.CreateInStream(xlInStr);
        xlNombreFichero := 'Vacunas.csv';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('No se ha podido descargar el fichero');
        end;
    end;

    local procedure ImportarVacunasF()
    var
        rlCabPlanVacunacionC01: Record D_CabPlanVacunacionC01;
        pgListaCabPlanVacunacionC01: Page D_ListaCabPlanVacunacionC01;
        rlLinPlanVacunacion: Record D_LinPlanVacunacionC01;
        TempLConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        xlInStr: InStream;
        xlLinea: Text;
        clSeparador: Label '.', Locked = true;
        xlFecha: Date;
        xlNumLinea: Integer;
    begin
        TempLConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
        if UploadIntoStream('', xlInStr) then begin
            while not xlInStr.EOS do begin
                xlInStr.ReadText(xlLinea);
                //Insertar cabecera o lineas dependiendo del primer carácter de la línea
                case true of
                    xlLinea[1] = 'C':
                        //Alta cabecera
                        begin
                            rlCabPlanVacunacionC01.Init();
                            rlCabPlanVacunacionC01.Validate(CodigoCabecera, xlLinea.Split(clSeparador).Get(2));
                            rlCabPlanVacunacionC01.Insert(True);
                            rlCabPlanVacunacionC01.Validate(Descripcion, xlLinea.Split(clSeparador).Get(3));
                            rlCabPlanVacunacionC01.Validate(EmpresaVacunadora, xlLinea.Split(clSeparador).Get(4));
                            if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(5)) then begin
                                rlCabPlanVacunacionC01.Validate(FechaInicioPlanifcVacunacion, xlFecha);
                            end;
                            rlCabPlanVacunacionC01.Modify(true);
                        end;
                    xlLinea[1] = 'L':
                        begin
                            rlLinPlanVacunacion.Init();
                            rlLinPlanVacunacion.Validate(CodigoLineas, xlLinea.Split(clSeparador).Get(2));

                            if Evaluate(xlNumLinea, xlLinea.Split(clSeparador).Get(8)) then begin

                                rlLinPlanVacunacion.Validate(NumLinea, xlNumLinea);
                            end;
                            rlLinPlanVacunacion.Insert(True);
                            rlLinPlanVacunacion.Validate(CodCteAVacunar, xlLinea.Split(clSeparador).Get(3));
                            rlLinPlanVacunacion.Validate(CodigoOtros, xlLinea.Split(clSeparador).Get(4));
                            rlLinPlanVacunacion.Validate(CodigoVacuna, xlLinea.Split(clSeparador).Get(5));
                            if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(6)) then begin
                                rlLinPlanVacunacion.Validate(FechaVacunacion, xlFecha);
                            end;
                            if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(7)) then begin
                                rlLinPlanVacunacion.Validate(FechaProximaVacunacion, xlFecha);
                            end;
                            rlLinPlanVacunacion.Modify(true);
                        end;
                    else
                        Error('Tipo de línea %1 no reconocido', xlLinea[1]);
                end;
            end;
        end else begin
            Error('No se ha podido subir el fichero al servidor');
        end;
    end;

    local procedure RecuperarClienteF()
    var
        rlCustomer: Record Customer;
    begin
        rlCustomer.Get('10000');
    end;

    local procedure RecuperarLineaPedidoVentaF()
    var
        rlSalesLine: Record "Sales Line";
    begin
        rlSalesLine.Get(rlSalesLine."Document Type"::Order, '101005', 10000);
    end;

    local procedure DimensionesF()
    var
        rlDimensionSetEntry: Record "Dimension Set Entry";
    begin
        rlDimensionSetEntry.Get(7, 'VENDEDOR');
    end;

    local procedure InfoEmpresaF()
    var
        rlInfoEmpresa: Record "Company Information";
    begin
        rlInfoEmpresa.Get();
    end;

    local procedure ClientesGrisF()
    var
        rlCustomer: Record Customer;
        xlFiltroGrupo: Integer;
        rlCompany: Record Company;
    begin
        //Mostrar la lista de empresas de la BBDD y de la seleccionada mostrar sus clientes grises
        if AccionAceptadaF(page.RunModal(Page::Companies, rlCompany)) then begin
            rlCustomer.ChangeCompany();
            xlFiltroGrupo := rlCustomer.FilterGroup();
            rlCustomer.FilterGroup(xlFiltroGrupo + 20);
            rlCustomer.SetRange("Location Code", 'GRIS');
            if AccionAceptadaF(page.RunModal(page::"Customer Link", rlCustomer)) then begin
                Message('Proceso OK');
            end else begin
                Error('Proceso cancelado');
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
    end;

    local procedure AccionAceptadaF(pAccion: Action): Boolean
    begin
        exit(pAccion in [pAccion::Yes, pAccion::OK, pAccion::LookupOK]);
    end;

    procedure ExportarPedidosVentaNikoF()   //Mérito de Niko
    var

        pglSalesOrderList: Page "Sales Order List";
        xlOutStr: OutStream;
        TempLMMConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        rlMMLinPlanVacunacionC01: Record "Sales Line";
        rlSalesHeader: Record "Sales Header";
        xlNombreFichero: Text;
        xlInStr: Instream;
        xlLinea: Text;
        culTypeHelper: Codeunit "Type Helper";
        xlSeparador: Label '·', Locked = true;
    begin
        CurrPage.SetSelectionFilter(rlSalesHeader);
        if rlSalesHeader.FindSet(false) then begin //SI MODIFICO (O BIEN LA CALVE PRINCIPAL O UN CAMPO QUE PERTENEZCA A ESE FILTRO HAY QUE PONER EL SEGUNDO TRUE): TRUE, TRUE
            TempLMMConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
            repeat
                xlOutStr.WriteText('C');
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."No.");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Document Type"));
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."Bill-to Name");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."Bill-to Contact");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."Bill-to Customer No.");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."Sell-to Customer No.");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Status"));
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Posting Date"));
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Due Date"));
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Shipment Date"));
                xlOutStr.WriteText();
                //RECORREMOS LAS LÍNEAS
                rlMMLinPlanVacunacionC01.SetRange("Document No.", rlSalesHeader."No.");
                rlMMLinPlanVacunacionC01.SetRange("Document Type", rlSalesHeader."Document Type");
                if rlMMLinPlanVacunacionC01.FindSet(false) then begin //SI MODIFICO (O BIEN LA CALVE PRINCIPAL O UN CAMPO QUE PERTENEZCA A ESE FILTRO HAY QUE PONER EL SEGUNDO TRUE): TRUE, TRUE
                    //1 p: campo por el que queremos filtrar el parametro. 2p: "DESDE" | VALOR, si no pongo el 3, estoy diciendo que filtrame el campo 1 con este VALOR. Si le ponemos el 3p: "HASTA".
                    //SETFILTER. 3 valores. Primero: Campo. Segundo: Cadena. Tecero: Valores.
                    repeat
                        xlOutStr.WriteText('L');
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlMMLinPlanVacunacionC01."Line No."));
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlMMLinPlanVacunacionC01."Document No."));
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlMMLinPlanVacunacionC01."Document Type"));
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlMMLinPlanVacunacionC01."Type"));
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(rlMMLinPlanVacunacionC01."No.");
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(rlMMLinPlanVacunacionC01.Description);
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlMMLinPlanVacunacionC01.Amount));
                        xlOutStr.WriteText()
                    until rlMMLinPlanVacunacionC01.Next() = 0;
                end;
            until rlSalesHeader.Next() = 0;



        end else begin
            Error('Proceso cancelado por el usuario');
        end;
        xlNombreFichero := 'PedidosVentas.csv';
        TempLMMConfiguracionC01.MiBlob.CreateInStream(xlInStr);
        xlInStr.ReadText(xlLinea);
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('Error 404');
        end;

    end;

    local procedure ImportarPedidosVentaNikoF() //Mérito de Niko
    var
        xlInStr: Instream;
        xlLinea: Text;
        rlSalesHeader: Record "Sales Header";
        rlSalesLine: Record "Sales Line";
        TempLConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        clSeparador: Label '·', Locked = true;
        xlFecha: Date;
        xlLNum: Integer;
        xlTipo: Enum "Sales Document Type";
        xlAmount: Decimal;
        xltipoL: Enum "Sales Line Type";
        xlStatus: Enum "Sales Document Status";
    begin
        TempLConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
        if UploadIntoStream('', xlInStr) then begin
            while not xlInStr.EOS do begin //EOS END OF STRING. LEE HASTA EL FINAL.
                xlInStr.ReadText(xlLinea);
                //Insertar Cabecera o líneas en funcion del primer caracter de la línea
                case true of
                    xlLinea[1] = 'C':
                        //DAR DE ALTA CABECERA
                        begin
                            rlSalesHeader.Init();
                            if Evaluate(xlTipo, xlLinea.Split(clSeparador).Get(3)) then begin
                                rlSalesHeader.Validate("Document Type", xlTipo);
                            end;
                            rlSalesHeader.Validate("No.", xlLinea.Split(clSeparador).Get(2));
                            rlSalesHeader.Insert(true);
                            rlSalesHeader.Validate("Bill-to Name", xlLinea.Split(clSeparador).Get(4));
                            rlSalesHeader.Validate("Bill-to Contact", xlLinea.Split(clSeparador).Get(5));
                            rlSalesHeader.Validate("Bill-to Customer No.", xlLinea.Split(clSeparador).Get(6));
                            rlSalesHeader.Validate("Sell-to Customer No.", xlLinea.Split(clSeparador).Get(7));

                            if Evaluate(xlStatus, xlLinea.Split(clSeparador).Get(8)) then begin
                                rlSalesHeader.Validate("Status", xlStatus);
                            end;

                            if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(9)) then begin
                                rlSalesHeader.Validate("Posting Date", xlFecha);
                            end;

                            if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(10)) then begin
                                rlSalesHeader.Validate("Due Date", xlFecha);
                            end;

                            if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(11)) then begin
                                rlSalesHeader.Validate("Shipment Date", xlFecha);
                            end;

                            rlSalesHeader.Modify(true);
                        end;
                    xlLinea[1] = 'L':
                        begin
                            rlSalesLine.Init();
                            if Evaluate(xlTipo, xlLinea.Split(clSeparador).Get(4)) then begin
                                rlSalesLine.Validate("Document Type", xlTipo);
                            end;
                            rlSalesLine.Validate("Document No.", xlLinea.Split(clSeparador).Get(3));
                            if Evaluate(xlLNum, xlLinea.Split(clSeparador).Get(2)) then begin
                                rlSalesLine.Validate("Line No.", xlLNum);
                            end;
                            rlSalesLine.Insert(true);
                            if Evaluate(xltipoL, xlLinea.Split(clSeparador).Get(5)) then begin
                                rlSalesLine.Validate(Type, xltipoL);
                            end;
                            rlSalesLine.Validate("No.", xlLinea.Split(clSeparador).Get(6));
                            rlSalesLine.Validate(Description, xlLinea.Split(clSeparador).Get(7));
                            if Evaluate(xlAmount, xlLinea.Split(clSeparador).Get(8)) then begin
                                rlSalesLine.Validate(Amount, xlAmount);
                            end;
                            rlSalesLine.Modify(true);

                        end;
                    else
                        Error('Tipo de línea %1 no reconocido', xlLinea[1]);
                end
            end;
        end else begin
            Error('No se ha podido subir el fichero al servidor. Error (%1', GetLastErrorText());
        end;
    end;

    local procedure ExportarPedidosVentaF()
    var
        rlSalesHeaderC01: Record "Sales Header";    //Cabecera de pedidos venta
        pgSalesOrderC01: Page TCNJFD_SalesHeaderC01;   //Pagina Sales Header
        rlSalesLineC01: Record "Sales Line";
        TempLConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        xlInStr: InStream;
        xlOutStr: OutStream;
        xlLinea: Text;
        xlNombreFichero: Text;
        clCadenaCabecera: Label 'C%1%2%1%3%1%4%1%5', Locked = true;
        clCadenaLinea: Label 'C%1%2%1%3%1%4%1%5%1%6%1%7%1%8', Locked = true;
        clSeparador: Label '·', Locked = true;
    begin
        TempLConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
        Message('Seleccione Pedidos de venta a exportar');
        pgSalesOrderC01.LookupMode(true);
        pgSalesOrderC01.SetRecord(rlSalesHeaderC01);
        if pgSalesOrderC01.RunModal() in [Action::OK, Action::Yes, Action::LookupOK] then begin
            pgSalesOrderC01.GetSelectionFilterF(rlSalesHeaderC01);
            //Obtenemos el dataset
            if rlSalesHeaderC01.FindSet(false) then begin
                repeat
                    xlOutStr.WriteText('C'); //Para identificar que es la cabecera
                    xlOutStr.WriteText(StrSubstNo(clCadenaCabecera, clSeparador,
                        rlSalesHeaderC01."No.", clSeparador,
                        rlSalesHeaderC01."Document Type", clSeparador,
                        rlSalesHeaderC01."Sell-to Customer No.", clSeparador,
                        rlSalesHeaderC01.Amount));
                    xlOutStr.WriteText();
                    //Recorremos las líneas de pedido para exportarlas
                    rlSalesLineC01.SetRange("Document No.", rlSalesHeaderC01."No.");
                    if rlSalesLineC01.FindSet(false) then begin
                        repeat
                            xlOutStr.WriteText('L'); //Para identificar que es la línea
                            xlOutStr.WriteText(StrSubstNo(clCadenaLinea, clSeparador,
                                rlSalesLineC01."Line No.", clSeparador,
                                rlSalesLineC01."Document No.", clSeparador,
                                rlSalesLineC01."Document Type", clSeparador,
                                rlSalesLineC01."Sell-to Customer No."));
                            xlOutStr.WriteText();
                        until rlSalesLineC01.Next() = 0;
                    end;
                until rlSalesHeaderC01.Next() = 0;
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
        TempLConfiguracionC01.MiBlob.CreateInStream(xlInStr);
        xlNombreFichero := 'PedidosVenta.csv';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('No se ha podido descargar el fichero');
        end;
    end;

    local procedure ImportarPedidosVentaF()
    var
        xlInStr: Instream;
        xlLinea: Text;
        rlSalesHeader: Record "Sales Header";
        rlSalesLine: Record "Sales Line";
        TempLConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        clSeparador: Label '·', Locked = true;
        xlFecha: Date;
        xlLNum: Integer;
        xlTipo: Enum "Sales Document Type";
        xlAmount: Decimal;
        xltipoL: Enum "Sales Line Type";
        xlStatus: Enum "Sales Document Status";
        xlEnum: Enum "Acc. Schedule Line Show";
        clError01: Text;
        TempDLogC01: Record TCNJFD_LogC01 temporary;
    begin
        TempLConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
        if UploadIntoStream('', xlInStr) then begin
            while not xlInStr.EOS do begin //EOS END OF STRING. LEE HASTA EL FINAL.
                xlInStr.ReadText(xlLinea);
                //Insertar Cabecera o líneas en funcion del primer caracter de la línea
                case true of
                    xlLinea[1] = 'C':
                        //DAR DE ALTA CABECERA
                        begin
                            rlSalesHeader.Init();
                            rlSalesHeader.Validate("No.", xlLinea.Split(clSeparador).Get(2));
                            if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(5)) then begin
                                rlSalesHeader.Validate("Document Type", xlEnum);
                            end;
                            rlSalesHeader.Insert(true);
                            if AsignaValoresCabeceraF(xlLinea, rlSalesHeader, xlAmount) then begin
                                rlSalesHeader.Modify(true);
                            end else begin
                                //InsertaIncidenciaF(StrSubstNo(clError01, rlSalesHeader."No.", GetLastErrorText(), TempDLogC01));
                            end;

                        end;
                    xlLinea[1] = 'L':
                        begin
                            rlSalesLine.Init();
                            if Evaluate(xlLNum, xlLinea.Split(clSeparador).Get(2)) then begin
                                rlSalesLine.Validate("Line No.", xlLNum);
                            end;
                            rlSalesLine.Validate("Document No.", xlLinea.Split(clSeparador).Get(3));
                            if Evaluate(xlTipo, xlLinea.Split(clSeparador).Get(8)) then begin
                                rlSalesLine.Validate("Document Type", xlTipo);
                            end;
                            rlSalesLine.Insert(true);
                            //rlSalesHeader.Validate("Sell-to Customer No.", xlLinea.Split(clSeparador).Get(4));
                            if Evaluate(xlAmount, xlLinea.Split(clSeparador).Get(11)) then begin
                                rlSalesLine.Validate(Amount, xlAmount);
                            end;
                            rlSalesLine.Modify(true);
                        end;
                    else
                        Error('Tipo de línea %1 no reconocido', xlLinea[1]);
                end
            end;
        end else begin
            Error('No se ha podido subir el fichero al servidor. Error (%1', GetLastErrorText());
        end;
    end;

    local procedure MuestraEmpresasF()
    var
        rlCompanyC01: Record Company;    //Empresa
        rlCustomer: Record Customer;
        rlVendor: Record Vendor;
        rlProductos: Record Item;
        pgCompaniesC01: Page Companies;   //Pagina Empresas
        TempLConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
    begin
        //TempLConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
        Message('Seleccione la empresa de la que quiere ver datos');
        pgCompaniesC01.LookupMode(true);
        pgCompaniesC01.SetRecord(rlCompanyC01);
        //if pgCompaniesC01.RunModal() in [Action::OK, Action::Yes, Action::LookupOK] then begin
        if rlCompanyC01.FindSet(false) then begin
            //rlCompanyC01.Get();
            if rlCustomer.FindSet(false) then begin
                rlCustomer.get(rlCustomer."No.");
            end;
            //rlCustomer.Get();
        end;
        //end;
    end;

    local procedure AsignaValoresCabeceraF(var xlLinea: Text; var rlSalesHeader: Record "Sales Header"; var xlAmount: Decimal): Boolean
    var
        clSeparador: Label '·', Locked = true;
    begin
        rlSalesHeader.Validate("Sell-to Customer No.", xlLinea.Split(clSeparador).Get(4));
        if Evaluate(xlAmount, xlLinea.Split(clSeparador).Get(5)) then begin
            rlSalesHeader.Validate(Amount, xlAmount);
        end;
    end;

    local procedure InsertaIncidenciaF(pTexto: Text; var TempDLogC01: Record TCNJFD_LogC01 temporary)
    var
    begin
        TempDLogC01.Init();
        //Sin terminar
    end;

    var
        xPassword: Text;
        xNumIteraciones: Integer;
}
