page 50159 "TCNJFD_EjDiccionarioC01"
{
    ApplicationArea = All;
    Caption = 'D_EjDiccionarioC01';
    PageType = List;
    SourceTable = Integer;
    UsageCategory = Lists;
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Num; Rec.Number)
                {
                    ApplicationArea = All;
                    Caption = 'Nº';
                }
                field(Campo01; ValorCeldaF(Rec.Number, 1))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 01';
                }
                field(Campo02; ValorCeldaF(Rec.Number, 2))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 02';
                }
                field(Campo03; ValorCeldaF(Rec.Number, 3))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 03';
                }
                field(Campo04; ValorCeldaF(Rec.Number, 4))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 04';
                }
                field(Campo05; ValorCeldaF(Rec.Number, 5))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 05';
                }
                field(Campo06; ValorCeldaF(Rec.Number, 6))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 06';
                }
                field(Campo07; ValorCeldaF(Rec.Number, 7))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 07';
                }
                field(Campo08; ValorCeldaF(Rec.Number, 8))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 08';
                }
                field(Campo09; ValorCeldaF(Rec.Number, 9))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 09';
                }
                field(Campo010; ValorCeldaF(Rec.Number, 10))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 10';
                }
            }
        }
    }

    local procedure ValorCeldaF(pFila: Integer; pClomuna: Integer): Decimal
    begin
        exit(xDicc.Get(pFila).Get(pClomuna));
    end;

    local procedure EjemploNotificacionF()
    var
        xlNotificacion: Notification;
        i: Integer;
    begin
        for i := 1 to 3 do begin
            xlNotificacion.Id(CreateGuid());
            xlNotificacion.Message(StrSubstNo('Carga %1 finalizada', i));
            xlNotificacion.Send();
        end;
        xlNotificacion.Id(CreateGuid());
        xlNotificacion.Message('El cliente no tiene ninguna vacuna');
        xlNotificacion.AddAction('Abrir lista de clientes', Codeunit::TCNJFD_FuncionesAppC01, 'AbrirListaClientesF');
        xlNotificacion.SetData('CodigoCliente', '10000');
        xlNotificacion.Send();
    end;

    trigger OnOpenPage()
    var
        xlFila: Integer;
        xlClomuna: Integer;
        xlNumeros: List of [Decimal];
    begin
        for xlFila := 1 to 10 do begin
            for xlClomuna := 1 to 10 do begin
                xlNumeros.Add(Random(55555));
            end;
            xDicc.Add(xlFila, xlNumeros);
        end;
        Rec.SetRange(Number, 1, 10);
        EjemploNotificacionF()
    end;

    var
        xDicc: Dictionary of [Integer, List of [Decimal]];
}
