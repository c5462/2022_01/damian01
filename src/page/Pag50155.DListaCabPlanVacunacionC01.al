page 50155 "D_ListaCabPlanVacunacionC01"
{
    ApplicationArea = All;
    Caption = 'Lista Cab. Plan Vacunacion';
    PageType = List;
    SourceTable = D_CabPlanVacunacionC01;
    UsageCategory = Lists;
    CardPageId = D_FichaCabPlanVacunacionC01;
    Editable = false;
    InsertAllowed = false;
    DeleteAllowed = false;
    ModifyAllowed = false;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Rec.CodigoCabecera)
                {
                    ToolTip = 'Specifies the value of the Codigo field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripcion field.';
                    ApplicationArea = All;
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    ToolTip = 'Specifies the value of the Empresa Vacunadora field.';
                    ApplicationArea = All;
                }
                field(FechaInicioPlanifcVacunacion; Rec.FechaInicioPlanifcVacunacion)
                {
                    ToolTip = 'Specifies the value of the Fecha Inicio Planificacion Vacunacion field.';
                    ApplicationArea = All;
                }
                field(NombreEmpresaVacunadora; Rec.NombreEmpresaVacunadoraF())
                {
                    Caption = 'Nombre empresa vacunadora';
                    ToolTip = 'Aparecerá el nombre de la empresa vacunadora';
                    ApplicationArea = All;
                }
            }
        }
    }
    procedure GetSelectionFilterF(var xSalida: Record D_CabPlanVacunacionC01)
    begin
        CurrPage.SetSelectionFilter(xSalida);
    end;
}
