/// <summary>
/// Page "TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJFD_SubLinPlanVacunaC01""""""""""""""""""""""""""""""" (ID 50156).
/// </summary>
page 50156 D_SubLinPlanVacunaC01
{
    Caption = 'D_SubLinPlanVacunaC01';
    PageType = ListPart;
    SourceTable = D_LinPlanVacunacionC01;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(CodCteAVacunar; Rec.CodCteAVacunar)
                {
                    ToolTip = 'Specifies the value of the Cod. cliente a vacunar field.';
                    ApplicationArea = All;
                }
                field(Codigo; Rec.CodigoLineas)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                    Visible = false;
                }
                field(FechaVacunacion; Rec.FechaVacunacion)
                {
                    ToolTip = 'Specifies the value of the Fecha vacunación field.';
                    ApplicationArea = All;
                }
                field(FechaProxVac; Rec.FechaProximaVacunacion)
                {
                    ToolTip = 'Specifies the value of the Fecha vacunación field.';
                    ApplicationArea = All;
                }

                field(NumLinea; Rec.NumLinea)
                {
                    ToolTip = 'Specifies the value of the Nº línea field.';
                    ApplicationArea = All;
                }
                field(TipoOtros; Rec.CodigoOtros)
                {
                    ToolTip = 'Specifies the value of the Tipo Otros field.';
                    ApplicationArea = All;
                }
                field(TipoVacuna; Rec.CodigoVacuna)
                {
                    ToolTip = 'Specifies the value of the Tipo Vacuna field.';
                    ApplicationArea = All;
                }
                field(NombreCliente; Rec.NombreClienteF())
                {
                    Caption = 'Nombre cliente';
                    ApplicationArea = All;
                }
                field(DescVacuna; Rec.DescVariosF(eDTipoC01::Vacuna, Rec.CodigoVacuna))
                {
                    Caption = 'Descuento vacuna';
                    ApplicationArea = All;
                }
                field(DescOtros; Rec.DescVariosF(eDTipoC01::otros, Rec.CodigoOtros))
                {
                    Caption = 'Descuento otros';
                    ApplicationArea = All;
                }
            }
        }
    }
    var
        eDTipoC01: Enum TCNJFTipoC01;

    trigger OnNewRecord(BelowxRec: Boolean)
    var
        rlD_CabPlanVacunacionC01: Record D_CabPlanVacunacionC01;
    begin
        if rlD_CabPlanVacunacionC01.Get(Rec.CodigoLineas) then begin
            Rec.Validate(FechaVacunacion, rlD_CabPlanVacunacionC01.FechaInicioPlanifcVacunacion);
        end;
    end;

    /*trigger OnOpenPage()
    var
        rlD_FechaProxVacunacion: Record D_CabPlanVacunacionC01;
    begin
        if rlD_FechaProxVacunacion.Get(Rec.FechaProximaVacunacion) then begin
            Rec.FechaProximaVacunacion;
        end;
    end;*/
}
