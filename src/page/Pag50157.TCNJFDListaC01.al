page 50157 "TCNJFD_ListaC01"
{
    ApplicationArea = All;
    Caption = 'Lista Logs';
    PageType = List;
    SourceTable = TCNJFD_LogC01;
    UsageCategory = Lists;
    InsertAllowed = false;
    ModifyAllowed = false;
    ShowFilter = true;
    SourceTableView = sorting(SystemCreatedAt) order(descending);

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {

                }
            }
        }
    }
}
