page 50161 "TCNJFDListaEmpresasC01"
{
    ApplicationArea = All;
    Caption = 'DListaEmpresasC01';
    PageType = List;
    SourceTable = Company;
    UsageCategory = Lists;









    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Business Profile Id"; Rec."Business Profile Id")
                {
                    ToolTip = 'Specifies the value of the Business Profile Id field.';
                    ApplicationArea = All;
                }
                field("Display Name"; Rec."Display Name")
                {
                    ToolTip = 'Specifies the display name that is defined for the company. If a display name is not defined, then the company name is used.';
                    ApplicationArea = All;
                }
                field("Evaluation Company"; Rec."Evaluation Company")
                {
                    ToolTip = 'Specifies that the company is for trial purposes only, and that a subscription has not been purchased.';
                    ApplicationArea = All;
                }
                field(Id; Rec.Id)
                {
                    ToolTip = 'Specifies the value of the Id field.';
                    ApplicationArea = All;
                }
                field(Name; Rec.Name)
                {
                    ToolTip = 'Specifies the name of a company that has been created in the current database.';
                    ApplicationArea = All;
                }
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                }
                field(SystemId; Rec.SystemId)
                {
                    ToolTip = 'Specifies the value of the SystemId field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                }
            }
        }
    }

    local procedure DatosTablaF(pRecVariant: Variant; pDrillDown: Boolean): Integer
    var
        xlRecRef: RecordRef;
    begin
        if pRecVariant.IsRecord then begin
            xlRecRef.GetTable(pRecVariant);
        end;
        if Rec.Name <> CompanyName then begin
            xlRecRef.ChangeCompany(Rec.Name);
        end;
        if pDrillDown then begin
            pRecVariant := xlRecRef;
            Page.Run(0, pRecVariant);
        end else begin
            exit(xlRecRef.Count);
        end;
    end;

    procedure EjemploRecordRefsF()
    var
        xlRecRef: RecordRef;
        xlFieldRef: FieldRef;
        i: Integer;
    begin
        xlRecRef.Open(Database::Item);
        if xlRecRef.FindSet(false) then begin
            repeat
                for i := 1 to xlRecRef.FieldCount do begin
                    Message('%1', xlRecRef.FieldIndex(i).Value);
                end;
                xlRecRef.Field(3).Validate(Format(xlRecRef.Field(3).Value).ToUpper());

                xlFieldRef := xlRecRef.Field(3);
                xlFieldRef.Validate(Format(xlFieldRef.Value).ToUpper());

                xlRecRef.Field(4).Validate();
                xlRecRef.Modify(true);
            until xlRecRef.Next() = 0;
        end;
    end;
}
