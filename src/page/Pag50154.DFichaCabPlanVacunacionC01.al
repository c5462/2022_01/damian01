page 50154 "D_FichaCabPlanVacunacionC01"
{
    Caption = 'Cab. Plan Vacunacion';
    PageType = Document;
    SourceTable = D_CabPlanVacunacionC01;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Codigo; Rec.CodigoCabecera)
                {
                    ToolTip = 'Specifies the value of the Codigo field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripcion field.';
                    ApplicationArea = All;
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    ToolTip = 'Specifies the value of the Empresa Vacunadora field.';
                    ApplicationArea = All;
                }
                field(NombreEmpresaVacunadora; Rec.NombreEmpresaVacunadoraF())
                {
                    Caption = 'Nombre empresa vacunadora';
                    ApplicationArea = All;
                }
                field(FechaInicioPlanifcVacunacion; Rec.FechaInicioPlanifcVacunacion)
                {
                    ToolTip = 'Specifies the value of the Fecha Inicio Planificacion Vacunacion field.';
                    ApplicationArea = All;
                }
            }
            part(Lineas; D_SubLinPlanVacunaC01)
            {
                ApplicationArea = all;
                SubPageLink = CodigoLineas = field(CodigoCabecera);
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(EjemploVariables01)
            {
                ApplicationArea = All;
                Caption = 'Ejemplo de varibles 01';
                trigger OnAction()
                var
                    culD_FuncionesAppC01: Codeunit TCNJFD_FuncionesAppC01;
                    xlTexto: Text;
                begin
                    xlTexto := 'A desayunar que son las 10:00';
                    culD_FuncionesAppC01.EjemploVariablesESF(xlTexto);
                    Message(xlTexto);
                end;
            }
            action(EjemploVariables02)
            {
                ApplicationArea = All;
                Caption = 'Ejemplo de varibles objetos';
                trigger OnAction()
                var
                    culD_FuncionesAppC01: Codeunit TCNJFD_FuncionesAppC01;
                    xlTexto: TextBuilder;
                begin
                    xlTexto.Append('A desayunar que son las 10:00');
                    culD_FuncionesAppC01.EjemploVariables02ESF(xlTexto);
                    Message(xlTexto.ToText());
                end;
            }
            action(Bingo)
            {
                ApplicationArea = All;
                Caption = 'Bingo Time';
                trigger OnAction()
                var
                    culD_FuncionesAppC01: Codeunit TCNJFD_FuncionesAppC01;
                begin
                    culD_FuncionesAppC01.BingoF();
                end;
            }
            action(PruebaFncSegPlano)
            {
                ApplicationArea = all;
                Caption = 'prueba segundo plano';
                trigger OnAction()
                var
                    xlSesionIniciada: Integer;
                begin
                    //Codeunit.Run(Codeunit::TCNJFD_CodeUnitLentaC01);
                    StartSession(xlSesionIniciada, Codeunit::TCNJFD_CodeUnitLentaC01);
                    Message('Proceso lanzado en segundo plano');
                end;
            }
            action(PruebaFncPrimerPlano)
            {
                ApplicationArea = all;
                Caption = 'prueba primer plano';
                trigger OnAction()
                var
                    xlSesionIniciada: Integer;
                begin
                    Codeunit.Run(Codeunit::TCNJFD_CodeUnitLentaC01);
                    Message('Proceso lanzado en primer plano');
                end;
            }
            action(Logs)
            {
                ApplicationArea = All;
                Caption = 'Log';
                Image = Log;
                RunObject = page TCNJFD_ListaC01;
            }
            action(CrearLineas)
            {
                ApplicationArea = All;
                Caption = 'Crear líneas';
                Image = Create;
                trigger OnAction()
                var
                    rlLinPlanVacunacion: Record D_LinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to 10000 do begin
                        rlLinPlanVacunacion.Init();
                        rlLinPlanVacunacion.Validate(CodigoLineas, Rec.CodigoCabecera);
                        rlLinPlanVacunacion.Validate(NumLinea, i);
                        rlLinPlanVacunacion.Insert(true);
                    end;
                end;
            }
            action(CrearLineasNoBulk)
            {
                ApplicationArea = All;
                Caption = 'Crear líneas no bulk';
                Image = Create;
                trigger OnAction()
                var
                    rlLinPlanVacunacion: Record D_LinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to 10000 do begin
                        rlLinPlanVacunacion.Init();
                        rlLinPlanVacunacion.Validate(CodigoLineas, Rec.CodigoCabecera);
                        rlLinPlanVacunacion.Validate(NumLinea, i);
                        if not rlLinPlanVacunacion.Insert(true) then begin
                            Error('No se ha podido insertar la línea');
                        end;
                    end;
                end;
            }
            action(ModificarLineas)
            {
                ApplicationArea = All;
                Caption = 'Crear líneas';
                Image = Create;
                trigger OnAction()
                var
                    rlLinPlanVacunacion: Record D_LinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    rlLinPlanVacunacion.SetRange(CodigoLineas, Rec.CodigoCabecera);
                    rlLinPlanVacunacion.ModifyAll(FechaVacunacion, Today + 1, true);
                    Message('Tiempo invertido %1', CurrentDateTime - xlInicio);
                end;
            }
            action(ModificarLineasNoBulk)
            {
                ApplicationArea = All;
                Caption = 'Crear líneas no bulk';
                Image = Create;
                trigger OnAction()
                var
                    rlLinPlanVacunacion: Record D_LinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    rlLinPlanVacunacion.SetRange(CodigoLineas, Rec.CodigoCabecera);
                    if rlLinPlanVacunacion.FindSet(true, false) then begin
                        repeat
                            rlLinPlanVacunacion.Validate(FechaVacunacion, Today + 1);
                            rlLinPlanVacunacion.Modify(true);
                        until rlLinPlanVacunacion.Next() = 0;
                    end;
                    Message('Tiempo invertido %1', CurrentDateTime - xlInicio);
                end;
            }
        }
    }
}
