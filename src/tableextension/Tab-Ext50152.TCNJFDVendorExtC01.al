tableextension 50152 "TCNJFD_VendorExtC01" extends Vendor
{
    trigger OnAfterInsert()
    var
        culD_VariablesGlobalesAppC01: Codeunit TCNJFD_VariablesGlobalesAppC01;
    begin
        Rec.Validate(Name, culD_VariablesGlobalesAppC01.NombreF());
    end;
}
