/// <summary>
/// TableExtension TCNJFCustomerExtC01 (ID 50100) extends Record Customer.
/// </summary>
tableextension 50150 "TCNJFCustomerExtC01" extends Customer
{

    fields
    {
        field(50100; TCNJFNoVacunaC01; Integer)
        {
            Caption = 'Nº de Vacunas';
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = count(D_LinPlanVacunacionC01 where(CodCteAVacunar = field("No.")));
        }

        field(50102; TCNJFFechaVacunaC01; Date)
        {
            Caption = 'Fecha ultima Vacuna';
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = max(D_LinPlanVacunacionC01.FechaVacunacion where(CodCteAVacunar = field("No.")));
        }

        field(50101; TCNJFTipoVacunaC01; Code[20])
        {
            Caption = 'Tipo de Vacuna';
            DataClassification = CustomerContent;
            TableRelation = TCNJFVariosC01.Codigo where(Tipo = const(Vacuna));
            trigger OnValidate()
            var
                rlTCNJFVariosC01: Record TCNJFVariosC01;
            begin
                if rlTCNJFVariosC01.Get(rlTCNJFVariosC01.Tipo::Vacuna, Rec.TCNJFTipoVacunaC01) then begin
                    if rlTCNJFVariosC01.Bloqueado then begin
                        rlTCNJFVariosC01.TestField(Bloqueado, false);
                    end;
                end;
            end;
        }
        modify(Name)
        {
            trigger OnAfterValidate()
            var
                culD_VariablesGlobalesAppC01: Codeunit TCNJFD_VariablesGlobalesAppC01;
            begin
                culD_VariablesGlobalesAppC01.NombreF(Rec.Name);
            end;
        }
    }
    trigger OnAfterInsert()
    begin
        SincronizarRegistrosF(xAccion::Insert);
    end;

    trigger OnAfterModify()
    begin
        SincronizarRegistrosF(xAccion::Modify);
    end;

    trigger OnAfterDelete()
    begin
        SincronizarRegistrosF(xAccion::Delete);
    end;

    local procedure AccionAceptadaF(pAccion: Action): Boolean
    begin
        exit(pAccion in [pAccion::Yes, pAccion::OK, pAccion::LookupOK]);
    end;

    local procedure SincronizarRegistrosF(pAccion: Option)
    var
        rlCompany: Record Company;
        rlCustomer: Record Customer;
    begin
        //Filtrar por empresas distinta a la que estamos
        rlCompany.SetFilter(Name, '<>%1', CompanyName);
        //Recorrer las empresas
        if rlCompany.FindSet(false) then begin
            repeat
                //Cambiar la varible local de registro de clientes
                rlCustomer.ChangeCompany(rlCompany.Name);
                //Asignar los valores de cliente a la variable local
                rlCustomer.Init();
                rlCustomer.Copy(Rec);
                case pAccion of
                    xAccion::Insert:
                        //Inserte el cliente
                        rlCustomer.Insert(false);   //Sin TRUE para que no cree los contactos, ya que los crea en la empresa en la que estamos
                    xAccion::Modify:
                        //Inserte el cliente
                        rlCustomer.Modify(false);   //Sin TRUE para que no cree los contactos, ya que los crea en la empresa en la que estamos
                    xAccion::Delete:
                        //Inserte el cliente
                        rlCustomer.Delete(false);   //Sin TRUE para que no cree los contactos, ya que los crea en la empresa en la que estamos
                end;
            until rlCompany.Next() = 0;
        end;
    end;

    var
        xAccion: Option Insert,Modify,Delete;
}
