table 50151 "TCNJFVariosC01"
{
    Caption = 'Varios';
    DataClassification = ToBeClassified;
    LookupPageId = TCNJFJFFichaVAriosC01;
    DrillDownPageId = TCNJFJFFichaVAriosC01;
    Extensible = true;
    fields
    {
        field(1; Tipo; Enum TCNJFTipoC01)
        {
            Caption = 'Tipo';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; Codigo; Code[20])
        {
            Caption = 'Código';
            DataClassification = OrganizationIdentifiableInformation;

        }
        field(3; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(4; Bloqueado; Boolean)
        {
            Caption = 'Bloqueado';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(5; PeriodoSigVacuna; DateFormula)
        {
            Caption = 'Periodo segunda vacunacion';
            DataClassification = OrganizationIdentifiableInformation;
        }
    }
    keys
    {
        key(PK; Tipo, Codigo)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Descripcion)
        {

        }
    }
    trigger OnDelete()
    begin
        Rec.TestField(Bloqueado, true);
        /*if not Rec.Bloqueado then begin
            Error('No se permite eliminar un registro no bloqueado');
        end;*/
    end;
}
