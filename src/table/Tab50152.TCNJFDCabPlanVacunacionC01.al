table 50152 "TCNJFD_CabPlanVacunacionC01"
{
    Caption = 'D_CabPlanVacunacion';
    DataClassification = ToBeClassified;
    LookupPageId = D_ListaCabPlanVacunacionC01;

    fields
    {
        field(1; CodCabecera; Code[20])
        {
            Caption = 'Codigo';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; Descripcion; Text[50])
        {
            Caption = 'Descripcion';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(3; FechaInicioPlanifcVacunacion; Date)
        {
            Caption = 'Fecha Inicio Planificacion Vacunacion';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(4; EmpresaVacunadora; Code[20])
        {
            Caption = 'Empresa Vacunadora';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = Vendor."No.";
        }
    }
    keys
    {
        key(PK; CodCabecera)
        {
            Clustered = true;
        }
    }


    /// <summary>
    /// NombreEmpresaVacunadoraF.
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure NombreEmpresaVacunadoraF(): Text
    var
        rlVendor: Record Vendor;
    begin
        if rlVendor.Get(Rec.EmpresaVacunadora) then begin
            exit(rlVendor.Name);
        end;
    end;

    trigger OnDelete()
    var
        rlLinPlanVacunacion: Record D_LinPlanVacunacionC01;
    begin
        rlLinPlanVacunacion.SetRange(CodigoLineas, Rec.CodCabecera);
        if not rlLinPlanVacunacion.IsEmpty then begin
            rlLinPlanVacunacion.DeleteAll(true);
        end;
    end;
}
