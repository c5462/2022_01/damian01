/// <summary>
/// Table "TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJF"TCNJFD_LinPlanVacunacionC01""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" (ID 50153).
/// </summary>
table 50153 D_LinPlanVacunacionC01
{
    Caption = 'Líneas Plan Vacunación';
    DataClassification = ToBeClassified;
    LookupPageId = D_SubLinPlanVacunaC01;
    DrillDownPageId = D_SubLinPlanVacunaC01;

    fields
    {
        field(1; CodigoLineas; Code[20])
        {
            Caption = 'Código';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; NumLinea; Integer)
        {
            Caption = 'Nº línea';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(3; CodCteAVacunar; Code[20])
        {
            Caption = 'Cod. cliente a vacunar';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = Customer."No.";
            trigger OnValidate()
            var
                rlD_CabeceraPlanVacunacionC01: Record D_CabPlanVacunacionC01;
            begin
                if Rec.FechaVacunacion = 0D then begin
                    if rlD_CabeceraPlanVacunacionC01.Get(rec.CodigoLineas) then begin
                        rec.Validate(FechaVacunacion, rlD_CabeceraPlanVacunacionC01.FechaInicioPlanifcVacunacion);
                    end;
                end;
            end;
        }
        field(4; FechaVacunacion; Date)
        {
            Caption = 'Fecha vacunación';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(5; CodigoVacuna; Code[20])
        {
            Caption = 'Tipo Vacuna';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = TCNJFVariosC01.Codigo where(Tipo = const(Vacuna), Bloqueado = const(false));
            trigger OnValidate()
            begin
                CalcularFechaProxVacunacionF();
            end;
        }
        field(6; CodigoOtros; Code[20])
        {
            Caption = 'Tipo Otros';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = TCNJFVariosC01.Codigo where(Tipo = const(otros), Bloqueado = const(false));
        }
        field(7; FechaProximaVacunacion; Date)
        {
            Caption = 'Fecha proxima vacunación';
            DataClassification = OrganizationIdentifiableInformation;
        }
    }
    keys
    {
        key(PK; CodigoLineas, NumLinea)
        {
            Clustered = true;
        }
    }

    /// <summary>
    /// NombreClienteF.
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure NombreClienteF(): Text
    var
        rlCustomer: Record Customer;
    begin
        if rlCustomer.Get(Rec.CodCteAVacunar) then begin
            exit(rlCustomer.Name);
        end;
    end;
    /// <summary>
    /// DescVariosF.
    /// </summary>
    /// <param name="pTipo">Enum TCNJFTipoC01.</param>
    /// <param name="pCodVarios">Code[20].</param>
    /// <returns>Return value of type Text.</returns>
    procedure DescVariosF(pTipo: Enum TCNJFTipoC01; pCodVarios: Code[20]): Text
    var
        rlJFVariosC01: Record TCNJFVariosC01;
    begin
        if rlJFVariosC01.Get(pTipo, pCodVarios) then begin
            exit(rlJFVariosC01.Descripcion);
        end;
    end;

    /// <summary>
    /// CalcularFechaProxVacunacionF.
    /// </summary>
    procedure CalcularFechaProxVacunacionF()
    var
        rlD_VariosC01: Record TCNJFVariosC01;
        isHandled: Boolean;
    begin
        OnBeforeCalcularFechaProximaVacunaF(Rec, xRec, rlD_VariosC01, isHandled, CurrFieldNo);
        if not isHandled then begin
            if Rec.FechaVacunacion <> 0D then begin
                if rlD_VariosC01.Get(rlD_VariosC01.Tipo::Vacuna, Rec.CodigoVacuna) then begin
                    Rec.Validate(FechaProximaVacunacion, CalcDate(rlD_VariosC01.PeriodoSigVacuna, Rec.FechaVacunacion));
                end;
            end;
            OnAfterCalcularFechaProximaVacunaF(Rec, xRec, rlD_VariosC01, CurrFieldNo);
        end;
    end;

    [IntegrationEvent(false, false)]
    local procedure OnBeforeCalcularFechaProximaVacunaF(Rec: Record D_LinPlanVacunacionC01; xRec: Record D_LinPlanVacunacionC01; rlD_VariosC01: Record TCNJFVariosC01; var isHandled: Boolean; pCurrFieldNo: Integer)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure OnAfterCalcularFechaProximaVacunaF(Rec: Record D_LinPlanVacunacionC01; xRec: Record D_LinPlanVacunacionC01; rlD_VariosC01: Record TCNJFVariosC01; pCurrFieldNo: Integer)
    begin
    end;
}
