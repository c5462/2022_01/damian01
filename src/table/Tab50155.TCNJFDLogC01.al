table 50155 "TCNJFD_LogC01"
{
    Caption = 'Log';
    DataClassification = ToBeClassified;

    fields
    {
        field(1; IdUnico; Guid)
        {
            Caption = 'IdUnico';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; Texto; Text[250])
        {
            Caption = 'Texto';
            DataClassification = OrganizationIdentifiableInformation;
        }
    }
    keys
    {
        key(PK; IdUnico)
        {
            Clustered = true;
        }
        key(k2; SystemCreatedAt)
        {
        }
    }
    trigger OnInsert()
    begin
        if IsNullGuid(Rec.IdUnico) then begin
            Rec.Validate(IdUnico, CreateGuid());
        end;
    end;
}