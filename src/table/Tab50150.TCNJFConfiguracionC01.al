table 50150 "TCNJFConfiguracionC01"
{
    Caption = 'Configuración de la app';
    DataClassification = OrganizationIdentifiableInformation;

    fields
    {
        field(1; Id; Code[10])
        {
            Caption = 'Id de registro';
            DataClassification = SystemMetadata;

        }
        field(2; CodClienteWeb; Code[20])
        {
            Caption = 'Cod cliente para web';
            DataClassification = SystemMetadata;
            TableRelation = Customer."No.";
            TestTableRelation = true;
            ValidateTableRelation = true;
        }

        field(3; TextoRegistro; Text[250])
        {
            Caption = 'Texto registro';
            DataClassification = SystemMetadata;
        }
        field(4; TipoDoc; Enum TCNJFTipoDocumentoC01)
        {
            Caption = 'Tipo documento';
            DataClassification = SystemMetadata;
        }
        field(5; NombreCliente; Text[100])
        {
            Caption = 'Nombre Cliente';
            FieldClass = FlowField;
            CalcFormula = lookup(Customer.Name where("No." = field(CodClienteWeb)));
            Editable = false;
        }
        field(6; TipoTabla; Enum TCNJFTipoTablaC01)
        {
            Caption = 'TipoTabla';
            DataClassification = SystemMetadata;
            trigger OnValidate()
            begin
                if xRec.TipoTabla <> Rec.TipoTabla then begin
                    Rec.Validate(CodTabla, '');
                end;
            end;

        }
        field(7; CodTabla; Code[20])
        {
            Caption = 'CodTabla';
            DataClassification = SystemMetadata;
            TableRelation = if (TipoTabla = const(Cliente)) Customer else
            if (TipoTabla = const(Contacto)) Contact else
            if (TipoTabla = const(Empleado)) Employee else
            if (TipoTabla = const(Proveedor)) Vendor else
            if (TipoTabla = const(Recurso)) Resource;
        }
        field(8; ColorFondo; Enum TCNJFColoresC01)
        {
            Caption = 'Color Fondo';
            DataClassification = SystemMetadata;
        }
        field(9; ColorLetra; Enum TCNJFColoresC01)
        {
            Caption = 'Color Letra';
            DataClassification = SystemMetadata;

        }
        field(10; Cod2; Code[20])
        {
            Caption = 'Código 2';
            DataClassification = SystemMetadata;

        }
        field(11; UnidadesDisponibles; Decimal)
        {
            Caption = 'Unidades disponibles';
            // FieldClass = FlowField;
            // CalcFormula = sum("Item Ledger Entry".Quantity where("Item No." = field(FiltroProducto), "Posting Date" = field(FiltroFecha), "Entry Type" = field(FiltroTipoMovimiento)));
        }
        field(12; FiltroProducto; Code[20])
        {
            TableRelation = Item."No.";
            Caption = 'Filtro producto';
            FieldClass = FlowField;
        }
        field(13; FiltroFecha; Date)
        {
            Caption = 'Filtro fecha';
            FieldClass = FlowField;
        }
        field(14; FiltroTipoMovimiento; Enum "Item Ledger Entry Type")
        {
            Caption = 'Filtro tipo movimiento';
            FieldClass = FlowField;
        }
        field(15; IdPassword; Guid)
        {
            DataClassification = SystemMetadata;
        }
        field(16; MiBlob; Blob)
        {
            DataClassification = SystemMetadata;
        }
    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Id, CodClienteWeb) { }

        fieldgroup(Brick; Id, CodClienteWeb) { }
    }
    /// <summary>
    /// Recupera el registro de la tabla actual y si no lo crea
    /// </summary>
    /// <returns>Return value of type Boolean.</returns>
    procedure GetF(): Boolean
    begin
        if not Rec.get() then begin
            Rec.Init();
            Rec.Insert(true);
        end;
        exit(true);
    end;
    /// <summary>
    /// Devuelve el nombre del cliente pasado en el parametro pCodCLiente. Si no existe devolvera un texto vacio
    /// </summary>
    /// <param name="pCodCliente"></param>
    /// <returns></returns>
    procedure NombreClienteF(pCodCliente: Code[20]): Text

    var
        rlCustomer: Record Customer;
    begin
        if rlCustomer.get(pCodCliente) then begin
            exit(rlCustomer.Name);
        end;
    end;

    procedure NombreTablaF(pTipoTabla: Enum TCNJFTipoTablaC01; pCodTabla: code[20]): Text
    var
        RLTCNJFConfiguracionC01: Record TCNJFConfiguracionC01;
        rlContact: Record Contact;
        rlEmployee: Record Employee;
        rlVendor: Record Vendor;
        rlResource: Record Resource;

    begin
        case
            pTipoTabla of
            pTipoTabla::Cliente:
                begin
                    exit(RLTCNJFConfiguracionC01.NombreClienteF(pCodTabla));
                end;
            pTipoTabla::Contacto:
                begin
                    if rlContact.get(pCodTabla) then begin
                        exit(rlContact.Name);
                    end;
                end;
            pTipoTabla::Empleado:
                begin
                    if rlEmployee.get(pCodTabla) then begin
                        exit(rlEmployee.FullName());
                    end;
                end;
            pTipoTabla::Proveedor:
                begin
                    if rlVendor.get(pCodTabla) then begin
                        exit(rlVendor.Name);
                    end;
                end;
            pTipoTabla::Recurso:
                begin
                    if rlResource.get(pCodTabla) then begin
                        exit(rlResource.Name);
                    end;
                end;
        end
    end;

    [NonDebuggable]
    procedure PasswordF(pPassword: Text)
    begin
        Rec.Validate(IdPassword, CreateGuid());
        Rec.Modify(true);
        IsolatedStorage.Set(Rec.IdPassword, pPassword, DataScope::Company);
    end;

    [NonDebuggable]
    procedure PasswordF() xSalida: Text
    begin
        if IsolatedStorage.Get(Rec.IdPassword, DataScope::Company, xSalida) then begin
            Error('No se encuentra el password');
        end;
    end;
}
